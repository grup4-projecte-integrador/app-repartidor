package grup4.projecte.integrador.batoilogicdeliveries.Activities;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationManager;
import android.os.Bundle;

import androidx.core.app.ActivityCompat;
import androidx.fragment.app.FragmentActivity;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import grup4.projecte.integrador.batoilogicdeliveries.Modelos.Albaran;
import grup4.projecte.integrador.batoilogicdeliveries.R;

public class EveryPointMapActivity extends FragmentActivity implements OnMapReadyCallback {

    private GoogleMap mMap;
    private String direccio;
    private String nombreMapa;
    private ArrayList<Albaran> rutes;
    private LocationManager locationManager;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ubicacio_casa);
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);


        Intent intent = getIntent();

        Bundle value = intent.getExtras().getBundle("bundle");
        rutes = new ArrayList<>();

        for (int i = 0; i < value.getInt("size"); i++) {
            rutes.add((Albaran) value.getSerializable("ss" + i));
        }

    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        try {
            mMap = googleMap;
            Location location = null;
            LatLng latLng = null;

            if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) {

                locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
                location = locationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);
            }
            latLng = new LatLng(location.getLatitude(), location.getLongitude());

            mMap.setMyLocationEnabled(true);
            mMap.getUiSettings().setMyLocationButtonEnabled(true);

            Geocoder geocoder = new Geocoder(this, Locale.getDefault());

            for (int i = 0; i < rutes.size(); i++) {


                direccio = rutes.get(i).getPedido().getCiudad().getNombre() +
                        "," + rutes.get(i).getPedido().getDireccion().getNombre() +
                        ", " + rutes.get(i).getPedido().getDireccion().getNumero();

                List<Address> addresses;
                addresses = geocoder.getFromLocationName(direccio, 1);

                if (addresses.size() > 0) {

                    double latitude = addresses.get(0).getLatitude();
                    double longitude = addresses.get(0).getLongitude();

                    LatLng punto = new LatLng(latitude, longitude);

                    nombreMapa = rutes.get(i).getPedido().getCliente().getNombre() + " " + rutes.get(i).getPedido().getCliente().getApellidos() +
                            ", " + rutes.get(i).getPedido().getDireccion().getNombre() +
                            " Nº" + rutes.get(i).getPedido().getDireccion().getNumero();

                    mMap.addMarker(new MarkerOptions().icon(BitmapDescriptorFactory.fromResource(R.drawable.iconopaquete)).position(punto).title(nombreMapa));

                }
            }

            float zoom = 12;
            mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(latLng, zoom));

        } catch (IOException e) {

            e.printStackTrace();
        }
    }


}

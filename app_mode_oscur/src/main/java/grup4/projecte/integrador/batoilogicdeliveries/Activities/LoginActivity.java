package grup4.projecte.integrador.batoilogicdeliveries.Activities;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.app.AppCompatDelegate;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.os.Bundle;
import android.os.StrictMode;
import android.preference.PreferenceManager;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLOutput;
import java.util.HashMap;
import java.util.List;

import grup4.projecte.integrador.batoilogicdeliveries.Conexion.ConexionBD;
import grup4.projecte.integrador.batoilogicdeliveries.Modelos.Repartidor;
import grup4.projecte.integrador.batoilogicdeliveries.R;

import static java.util.Arrays.asList;

public class LoginActivity extends AppCompatActivity {

    public Button btEntrar;
    public EditText etUser;
    public EditText etPassword;

    private Connection con;
    private boolean isNightModeEnabled;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);

        ConexionBD.client = ConexionBD.getInstance();

        setUI();
    }


    public void setUI() {
        int currentNightMode = getResources().getConfiguration().uiMode & Configuration.UI_MODE_NIGHT_MASK;
//        switch (currentNightMode) {
//            case Configuration.UI_MODE_NIGHT_NO:
//                AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_NO);
//                setTheme(R.style.AppTheme_NoActionBar_Dark);
//                System.out.println("no mode nit");
//                break;
//            case Configuration.UI_MODE_NIGHT_YES:
//                AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_YES);
//                setTheme(R.style.AppTheme_NoActionBar_Light);
//                System.out.println("mode nit");
//                break;
//
//            case Configuration.UI_MODE_NIGHT_UNDEFINED:
//                // We don't know what mode we're in, assume notnight
//                break;
//        }

        etUser = findViewById(R.id.etUser);

        etPassword = findViewById(R.id.etPassword);

        btEntrar = findViewById(R.id.btEntrar);

        btEntrar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                try {
                    Repartidor repartidor = findByPK(etUser.getText().toString(), etPassword.getText().toString());

                    if (repartidor != null) {
                        startActivity(new Intent(getApplicationContext(), MainActivity.class));

                    } else {

                        hideKeyboardFrom(getApplicationContext(), v);
                        Toast.makeText(getApplicationContext(), "Usuario no encontrado", Toast.LENGTH_LONG).show();
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });

    }

    public Repartidor findByPK(String dni, String password) throws Exception {

        Repartidor r = new Repartidor();
        List l = asList((Object[]) ConexionBD.client.execute("execute_kw", asList(
                ConexionBD.DB, ConexionBD.uid, ConexionBD.PASS,
                "batoilogic.repartidor", "search_read",
                asList(asList(

                )), new HashMap() {
                    {
                        put("fields", asList("dni", "password"));
                        put("limit", 2);
                    }
                }
        )));

        for (int i = 0; i < l.size(); i++) {


        }

        return null;
    }

    public static void hideKeyboardFrom(Context context, View view) {
        InputMethodManager imm = (InputMethodManager) context.getSystemService(Activity.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
    }
}

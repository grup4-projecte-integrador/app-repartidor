package grup4.projecte.integrador.batoilogicdeliveries.Modelos;

import java.io.Serializable;
import java.util.Date;

public class Cliente implements Serializable {

    public String apellidos;
    public String nombre;
    public Date fechaNacimiento;

    public Cliente(String apellidos, String nombre, Date fechaNacimiento) {
        this.apellidos = apellidos;
        this.nombre = nombre;
        this.fechaNacimiento = fechaNacimiento;
    }

    public String getApellidos() {
        return apellidos;
    }

    public void setApellidos(String apellidos) {
        this.apellidos = apellidos;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public Date getFechaNacimiento() {
        return fechaNacimiento;
    }

    public void setFechaNacimiento(Date fechaNacimiento) {
        this.fechaNacimiento = fechaNacimiento;
    }
}

package grup4.projecte.integrador.batoilogicdeliveries.Conexion;

import org.apache.xmlrpc.XmlRpcException;
import org.apache.xmlrpc.client.XmlRpcClient;
import org.apache.xmlrpc.client.XmlRpcClientConfigImpl;

import java.net.MalformedURLException;
import java.net.URL;

import static java.util.Arrays.asList;
import static java.util.Collections.emptyMap;

public class ConexionBD {

    public static XmlRpcClient client;
    public static final String DB = "batoilogic";
    public static final String USER = "admin@grup4.es";
    public static final String PASS = "1234";
    public static int uid = 0;

    public static XmlRpcClient getInstance() {

        try {
            if (client == null) {
                client = new XmlRpcClient();

                XmlRpcClientConfigImpl config = new XmlRpcClientConfigImpl();
                config.setEnabledForExtensions(true);
                config.setServerURL(new URL("http", "grup4.cipfpbatoi.es", 80, "/xmlrpc/2/object"));

                client.setConfig(config);

                config = new XmlRpcClientConfigImpl();
                config.setEnabledForExtensions(true);
                config.setServerURL(new URL("http", "grup4.cipfpbatoi.es", 80, "/xmlrpc/2/common"));

                uid = (int) client.execute(config, "authenticate", asList(DB, USER, PASS, emptyMap()));
            }

        } catch (MalformedURLException ex) {

            ex.printStackTrace();

        } catch (XmlRpcException e) {
            e.printStackTrace();
        }

        return client;
    }


}

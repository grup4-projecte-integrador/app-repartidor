package grup4.projecte.integrador.batoilogicdeliveries.Activities;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.view.View;
import android.widget.Toast;

import com.google.android.material.floatingactionbutton.FloatingActionButton;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;

import grup4.projecte.integrador.batoilogicdeliveries.Modelos.Albaran;
import grup4.projecte.integrador.batoilogicdeliveries.Modelos.Ciudad;
import grup4.projecte.integrador.batoilogicdeliveries.Modelos.Cliente;
import grup4.projecte.integrador.batoilogicdeliveries.Modelos.Direccion;
import grup4.projecte.integrador.batoilogicdeliveries.Modelos.Pedido;
import grup4.projecte.integrador.batoilogicdeliveries.Modelos.Ruta;
import grup4.projecte.integrador.batoilogicdeliveries.R;

public class AlbaranActivity extends AppCompatActivity implements MyAdapter.OnItemClickListener, MyAdapter.OnLongItemClickListener {

    private ArrayList<Albaran> myDataset;
    private RecyclerView recyclerView;
    private RecyclerView.Adapter mAdapter;
    private RecyclerView.LayoutManager layoutManager;
    private static final int LOCATION_REQUEST = 1337 + 3;
    private static final String[] INITIAL_PERMS = {
            Manifest.permission.ACCESS_FINE_LOCATION,
            Manifest.permission.READ_CONTACTS
    };


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_albaran);

        setUI();
        if (!canAccessLocation()) {
            requestPermissions(INITIAL_PERMS, LOCATION_REQUEST);
        }
    }

    public void setUI() {

        FloatingActionButton fab = findViewById(R.id.fabMap);

        myDataset = new ArrayList<>();

        Cliente cli = new Cliente("Rozas", "Alejandro", new Date());
        Direccion dir = new Direccion("Passeig del Comtat", 9, 1, "A", "Calle");
        Direccion dir2 = new Direccion("Passatge Literat Azorín", 9, 1, "A", "Calle");
        Ciudad ciudad = new Ciudad(1, "Cocentaina");
        Pedido p = new Pedido(0, cli, dir, ciudad, 20, "Silla Gaming", "", "Preparando");
        Pedido p2 = new Pedido(0, cli, dir2, ciudad, 20, "Silla Gaming", "", "Preparando");
        myDataset.add(new Albaran(1, p, new Ruta(1, 20, 20)));
        myDataset.add(new Albaran(2, p2, new Ruta(1, 20, 20)));
        myDataset.add(new Albaran(3, p, new Ruta(1, 20, 20)));
        myDataset.add(new Albaran(4, p, new Ruta(1, 20, 20)));

        recyclerView = findViewById(R.id.recyclerViewActivities);
        recyclerView.setHasFixedSize(true);

        layoutManager = new LinearLayoutManager(this, RecyclerView.VERTICAL, false);
        recyclerView.setLayoutManager(layoutManager);

        mAdapter = new MyAdapter(myDataset, this, this);
        recyclerView.setAdapter(mAdapter);

        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(getApplicationContext(), EveryPointMapActivity.class);
                Bundle bundle = new Bundle();

                for (int i = 0; i < myDataset.size(); i++) {
                    bundle.putSerializable("ss" + i, (Serializable) myDataset.get(i));
                }

                bundle.putInt("size", myDataset.size());
                intent.putExtra("bundle", bundle);
                startActivity(intent);
            }
        });
    }

    @Override
    public void onItemClick(Albaran item) {
        Intent intent = new Intent(getApplicationContext(), SinglePointMapActivity.class);

        intent.putExtra("NombreCalle", item.getPedido().getDireccion().getNombre());
        intent.putExtra("Ciudad", item.getPedido().getCiudad().getNombre());
        intent.putExtra("Numero", item.getPedido().getDireccion().getNumero());
        intent.putExtra("NombreCliente", item.getPedido().getCliente().getNombre());
        intent.putExtra("ApellidosCliente", item.getPedido().getCliente().getApellidos());

        startActivity(intent);
    }


    private boolean canAccessLocation() {
        return (hasPermission(Manifest.permission.ACCESS_FINE_LOCATION));
    }

    private boolean hasPermission(String perm) {
        return (PackageManager.PERMISSION_GRANTED == checkSelfPermission(perm));
    }

    @Override
    public boolean onLongItemClick(Albaran item) {

        Intent intent = new Intent(getApplicationContext(), ModifyAlbaranActivity.class);

        intent.putExtra("Item", item);

        //        intent.putExtra("NombreCliente", item.getPedido().getCliente().getNombre());
//        intent.putExtra("IdPedido", item.getPedido().getId());
//
//        if (item.getPedido().getEstado().equals("Entregado")) {
//
//            intent.putExtra("EstadoPedido", true);
//
//        } else {
//
//            intent.putExtra("EstadoPedido", false);
//
//        }
//
//        if(!item.getPedido().getObservaciones().isEmpty()) {
//
//            intent.putExtra("Observaciones",  item.getPedido().getObservaciones());
//
//        }

        startActivity(intent);
        return true;
    }
}

package grup4.projecte.integrador.batoilogicdeliveries.Modelos;

import android.os.Parcel;
import android.os.Parcelable;

import java.io.Serializable;

public class Albaran implements Serializable{

    public int id;
    public Pedido pedido;
    public Ruta ruta;

    public Albaran(int id, Pedido pedido, Ruta ruta) {
        this.id = id;
        this.pedido = pedido;
        this.ruta = ruta;
    }


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Pedido getPedido() {
        return pedido;
    }

    public void setPedido(Pedido pedido) {
        this.pedido = pedido;
    }

    public Ruta getRuta() {
        return ruta;
    }

    public void setRuta(Ruta ruta) {
        this.ruta = ruta;
    }


}

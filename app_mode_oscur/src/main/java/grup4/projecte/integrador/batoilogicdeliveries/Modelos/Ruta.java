package grup4.projecte.integrador.batoilogicdeliveries.Modelos;

import java.io.Serializable;

public class Ruta implements Serializable {

    public int id;
    public int kilometrosEstimados;
    public int tiempoEstimado;


    public Ruta(int id, int kilometrosEstimados, int tiempoEstimado) {
        this.id = id;
        this.kilometrosEstimados = kilometrosEstimados;
        this.tiempoEstimado = tiempoEstimado;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getKilometrosEstimados() {
        return kilometrosEstimados;
    }

    public void setKilometrosEstimados(int kilometrosEstimados) {
        this.kilometrosEstimados = kilometrosEstimados;
    }

    public int getTiempoEstimado() {
        return tiempoEstimado;
    }

    public void setTiempoEstimado(int tiempoEstimado) {
        this.tiempoEstimado = tiempoEstimado;
    }
}

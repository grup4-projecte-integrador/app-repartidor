package grup4.projecte.integrador.batoilogicdeliveries.Modelos;

import java.util.Date;

public class Pedido {

    public int id;
    public Cliente cliente;
    public Direccion direccion;
    public Ciudad ciudad;
    public float precioTotal;
    public String descripcion;
    public String observaciones;
    public String estado;

    public Pedido(int id, Cliente cliente, Direccion direccion, Ciudad ciudad, float precioTotal, String descripcion, String observaciones, String estado) {
        this.id = id;
        this.cliente = cliente;
        this.direccion = direccion;
        this.ciudad = ciudad;
        this.precioTotal = precioTotal;
        this.descripcion = descripcion;
        this.observaciones = observaciones;
        this.estado = estado;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Cliente getCliente() {
        return cliente;
    }

    public void setCliente(Cliente cliente) {
        this.cliente = cliente;
    }

    public Direccion getDireccion() {
        return direccion;
    }

    public void setDireccion(Direccion direccion) {
        this.direccion = direccion;
    }

    public Ciudad getCiudad() {
        return ciudad;
    }

    public void setCiudad(Ciudad ciudad) {
        this.ciudad = ciudad;
    }

    public float getPrecioTotal() {
        return precioTotal;
    }

    public void setPrecioTotal(float precioTotal) {
        this.precioTotal = precioTotal;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public String getObservaciones() {
        return observaciones;
    }

    public void setObservaciones(String observaciones) {
        this.observaciones = observaciones;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }
}

package grup4.projecte.integrador.batoilogicdeliveries;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;

import java.util.ArrayList;
import java.util.Date;

import grup4.projecte.integrador.batoilogicdeliveries.Modelos.Albaran;
import grup4.projecte.integrador.batoilogicdeliveries.Modelos.Ciudad;
import grup4.projecte.integrador.batoilogicdeliveries.Modelos.Cliente;
import grup4.projecte.integrador.batoilogicdeliveries.Modelos.Direccion;
import grup4.projecte.integrador.batoilogicdeliveries.Modelos.Pedido;
import grup4.projecte.integrador.batoilogicdeliveries.Modelos.Ruta;

public class AlbaranActivity extends AppCompatActivity implements MyAdapter.OnItemClickListener {

    private ArrayList<Albaran> myDataset;
    private RecyclerView recyclerView;
    private RecyclerView.Adapter mAdapter;
    private RecyclerView.LayoutManager layoutManager;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_albaran);

        setUI();
    }

    public void setUI() {

        myDataset = new ArrayList<>();

        Cliente cli = new Cliente("Rozas", "Alejandro", new Date());
        Direccion dir = new Direccion("Calle 1", 9, 1 , "A", "Calle");
        Ciudad ciudad = new Ciudad(1, "Cocentaina");
        Pedido p = new Pedido(0, cli, dir,  ciudad, 20, "Silla Gaming", "", "Preparando");
        myDataset.add(new Albaran(1, p, new Ruta(1, 20, 20)));
        myDataset.add(new Albaran(1, p, new Ruta(1, 20, 20)));
        myDataset.add(new Albaran(1, p, new Ruta(1, 20, 20)));
        myDataset.add(new Albaran(1, p, new Ruta(1, 20, 20)));

        recyclerView = findViewById(R.id.recyclerViewActivities);
        recyclerView.setHasFixedSize(true);

        layoutManager = new LinearLayoutManager(this, RecyclerView.VERTICAL, false);
        recyclerView.setLayoutManager(layoutManager);

        mAdapter = new MyAdapter(myDataset, this);
        recyclerView.setAdapter(mAdapter);




    }

    @Override
    public void onItemClick(Albaran item) {

    }
}

package grup4.projecte.integrador.batoilogicdeliveries.Modelos;

public class Direccion {

    public String nombre;
    public int numero;
    public int piso;
    public String puerta;
    public String tipo;

    public Direccion(String nombre, int numero, int piso, String puerta, String tipo) {
        this.nombre = nombre;
        this.numero = numero;
        this.piso = piso;
        this.puerta = puerta;
        this.tipo = tipo;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public int getNumero() {
        return numero;
    }

    public void setNumero(int numero) {
        this.numero = numero;
    }

    public int getPiso() {
        return piso;
    }

    public void setPiso(int piso) {
        this.piso = piso;
    }

    public String getPuerta() {
        return puerta;
    }

    public void setPuerta(String puerta) {
        this.puerta = puerta;
    }

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }
}

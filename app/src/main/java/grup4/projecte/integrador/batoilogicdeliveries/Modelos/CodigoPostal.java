package grup4.projecte.integrador.batoilogicdeliveries.Modelos;

import java.io.Serializable;

public class CodigoPostal implements Serializable {
    int id;
    String codigo;
    Ciudad ciudad;

    public CodigoPostal(int id, String codigo, Ciudad ciudad) {
        this.id = id;
        this.codigo = codigo;
        this.ciudad = ciudad;
    }

    public CodigoPostal() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getCodigo() {
        return codigo;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    public Ciudad getCiudad() {
        return ciudad;
    }

    public void setCiudad(Ciudad ciudad) {
        this.ciudad = ciudad;
    }
}

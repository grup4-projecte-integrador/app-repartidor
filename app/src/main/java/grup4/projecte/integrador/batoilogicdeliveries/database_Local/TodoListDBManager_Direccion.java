package grup4.projecte.integrador.batoilogicdeliveries.database_Local;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import java.util.ArrayList;

import grup4.projecte.integrador.batoilogicdeliveries.Modelos_Internos.Direccion;


public class TodoListDBManager_Direccion {
    private TodoListDBHelper todoListDBHelper;

    public TodoListDBManager_Direccion(Context context) {
        todoListDBHelper = TodoListDBHelper.getInstance(context);
    }

    // Operations

    // CREATE new ros
    public void insertDireccion(Direccion direccion) {
        // open database to read and write
        SQLiteDatabase sqLiteDatabase = todoListDBHelper.getWritableDatabase();
        int id;
        String nombre = direccion.getNombre();
        String tipo = direccion.getTipo();
        int numero = direccion.getNumero();
        int piso = direccion.getPiso();
        String puerta = direccion.getPuerta();
        int cliente_id = direccion.getId_cliente();
        int postal_id = direccion.getCodigo_postal();
        if (sqLiteDatabase != null) {
            ContentValues contentValue = new ContentValues();

            contentValue.put(TodoListDBContract.Direcciones.NOMBRE, nombre);
            contentValue.put(TodoListDBContract.Direcciones.TIPO, tipo);
            contentValue.put(TodoListDBContract.Direcciones.NUMERO, numero);
            contentValue.put(TodoListDBContract.Direcciones.PISO, piso);
            contentValue.put(TodoListDBContract.Direcciones.PUERTA, puerta);
            contentValue.put(TodoListDBContract.Direcciones.CLIENTE_ID, cliente_id);
            contentValue.put(TodoListDBContract.Direcciones.POSTAL_ID, postal_id);


            sqLiteDatabase.insert(TodoListDBContract.Direcciones.TABLE_NAME, null, contentValue);

            // getWritableDatabase & getReadableDatabase() are expensive, you should leave your database connection
            // open for as long as you possibly need to access it
            //
            // sqLiteDatabase.close();
        }
    }


    // Get all data from Direcciones table
    public ArrayList<Direccion> getDirecciones() {
        ArrayList<Direccion> DireccionList = new ArrayList<>();

        // open database to read
        SQLiteDatabase sqLiteDatabase = todoListDBHelper.getReadableDatabase();

        if (sqLiteDatabase != null) {
            String[] projection = new String[]{TodoListDBContract.Direcciones._ID,
                    TodoListDBContract.Direcciones.NOMBRE,
                    TodoListDBContract.Direcciones.TIPO,
                    TodoListDBContract.Direcciones.NUMERO,
                    TodoListDBContract.Direcciones.PISO,
                    TodoListDBContract.Direcciones.PUERTA,
                    TodoListDBContract.Direcciones.CLIENTE_ID,
                    TodoListDBContract.Direcciones.POSTAL_ID

            };


            Cursor cursorTodoList = sqLiteDatabase.query(TodoListDBContract.Direcciones.TABLE_NAME,
                    projection,                     // The columns toView return
                    null,                  // no WHERE clause
                    null,               // no values for the WHERE clause
                    null,                   // don't group hte rows
                    null,                    // don't filter by row groups
                    null);                  // don't sort rows


            if (cursorTodoList != null) {
                // get the column indexes for required columns
                int _idIndex = cursorTodoList.getColumnIndexOrThrow(TodoListDBContract.Direcciones._ID);
                int nombreIndex = cursorTodoList.getColumnIndexOrThrow(TodoListDBContract.Direcciones.NOMBRE);
                int tipoIndex = cursorTodoList.getColumnIndexOrThrow(TodoListDBContract.Direcciones.TIPO);
                int numeroIndex = cursorTodoList.getColumnIndexOrThrow(TodoListDBContract.Direcciones.NUMERO);
                int pisoIndex = cursorTodoList.getColumnIndexOrThrow(TodoListDBContract.Direcciones.PISO);
                int puertaIndex = cursorTodoList.getColumnIndexOrThrow(TodoListDBContract.Direcciones.PUERTA);
                int clienteIndex = cursorTodoList.getColumnIndexOrThrow(TodoListDBContract.Direcciones.CLIENTE_ID);
                int postalIndex = cursorTodoList.getColumnIndexOrThrow(TodoListDBContract.Direcciones.POSTAL_ID);

                //read data and add to ArrayList
                while (cursorTodoList.moveToNext()) {
                    Direccion Direccion = new Direccion(cursorTodoList.getInt(_idIndex),
                            cursorTodoList.getString(nombreIndex),
                            cursorTodoList.getInt(numeroIndex),
                            cursorTodoList.getInt(pisoIndex),
                            cursorTodoList.getString(puertaIndex),
                            cursorTodoList.getString(tipoIndex),
                            cursorTodoList.getInt(clienteIndex),
                            cursorTodoList.getInt(postalIndex)
                    );

                    DireccionList.add(Direccion);
                }
                // close cursor to free resources
                cursorTodoList.close();
            }
        }
        return DireccionList;
    }

    public void close() {
        todoListDBHelper.close();       // closes any opened database object
    }

    /**
     * Aci actualitzem el contingut de cada element al obrir el EditTaskActivity
     */
    public void update(Direccion direccion) {
        SQLiteDatabase sqLiteDatabase = todoListDBHelper.getWritableDatabase();
        int id = direccion.getId();
        String nombre = direccion.getNombre();
        String tipo = direccion.getTipo();
        int numero = direccion.getNumero();
        int piso = direccion.getPiso();
        String puerta = direccion.getPuerta();
        int cliente_id = direccion.getId_cliente();
        int postal_id = direccion.getCodigo_postal();
        if (sqLiteDatabase != null) {
            ContentValues contentValue = new ContentValues();

            contentValue.put(TodoListDBContract.Direcciones.NOMBRE, nombre);
            contentValue.put(TodoListDBContract.Direcciones.TIPO, tipo);
            contentValue.put(TodoListDBContract.Direcciones.NUMERO, numero);
            contentValue.put(TodoListDBContract.Direcciones.PISO, piso);
            contentValue.put(TodoListDBContract.Direcciones.PUERTA, puerta);
            contentValue.put(TodoListDBContract.Direcciones.CLIENTE_ID, cliente_id);
            contentValue.put(TodoListDBContract.Direcciones.POSTAL_ID, postal_id);


            sqLiteDatabase.update(TodoListDBContract.Direcciones.TABLE_NAME, contentValue, "_id=" + id, null);


        }
    }

    public void deleteDirecciones(int id) {
        SQLiteDatabase sqLiteDatabase = todoListDBHelper.getWritableDatabase();
        if (sqLiteDatabase != null) {
            sqLiteDatabase.delete(TodoListDBContract.Direcciones.TABLE_NAME, "_id=" + id, null);

        }

    }

    public void deleteComplDirecciones() {
        SQLiteDatabase sqLiteDatabase = todoListDBHelper.getWritableDatabase();
        String stat = "Completed";
        if (sqLiteDatabase != null) {
//            sqLiteDatabase.delete(TodoListDBContract.Direcciones.TABLE_NAME, TodoListDBContract.Direcciones.STATUS + "=" + stat, null);
            sqLiteDatabase.execSQL("DELETE FROM " + TodoListDBContract.Direcciones.TABLE_NAME + " WHERE status='Completed'");
        }
    }

    public void deleteAllDirecciones() {
        SQLiteDatabase sqLiteDatabase = todoListDBHelper.getWritableDatabase();
        if (sqLiteDatabase != null) {
            sqLiteDatabase.delete(TodoListDBContract.Direcciones.TABLE_NAME, null, null);

        }

    }


    public ArrayList<Direccion> getSelection(String stat) {
        ArrayList<Direccion> DireccionList = new ArrayList<>();
        String[] tipus = new String[]{stat};
        System.out.println("\t\t\t---------------->" + stat);

        // open database to read
        SQLiteDatabase sqLiteDatabase = todoListDBHelper.getReadableDatabase();

        if (sqLiteDatabase != null) {
            String[] projection = new String[]{TodoListDBContract.Direcciones._ID,
                    TodoListDBContract.Direcciones.NOMBRE,
                    TodoListDBContract.Direcciones.TIPO,
                    TodoListDBContract.Direcciones.NUMERO,
                    TodoListDBContract.Direcciones.PISO,
                    TodoListDBContract.Direcciones.PUERTA,
                    TodoListDBContract.Direcciones.CLIENTE_ID,
                    TodoListDBContract.Direcciones.POSTAL_ID
            };
//
//            taskList.clear();
            System.out.println(DireccionList.size());
//
            Cursor cursorTodoList = sqLiteDatabase.query(TodoListDBContract.Direcciones.TABLE_NAME,
                    projection,                     // The columns toView return
                    "status=?",                  // no WHERE clause
                    tipus,               // no values for the WHERE clause
                    null,                   // don't group hte rows
                    null,                    // don't filter by row groups
                    null);                  // don't sort rows


            if (cursorTodoList != null) {
                // get the column indexes for required columns
                int _idIndex = cursorTodoList.getColumnIndexOrThrow(TodoListDBContract.Direcciones._ID);
                int nombreIndex = cursorTodoList.getColumnIndexOrThrow(TodoListDBContract.Direcciones.NOMBRE);
                int tipoIndex = cursorTodoList.getColumnIndexOrThrow(TodoListDBContract.Direcciones.TIPO);
                int numeroIndex = cursorTodoList.getColumnIndexOrThrow(TodoListDBContract.Direcciones.NUMERO);
                int pisoIndex = cursorTodoList.getColumnIndexOrThrow(TodoListDBContract.Direcciones.PISO);
                int puertaIndex = cursorTodoList.getColumnIndexOrThrow(TodoListDBContract.Direcciones.PUERTA);
                int clienteIndex = cursorTodoList.getColumnIndexOrThrow(TodoListDBContract.Direcciones.CLIENTE_ID);
                int postalIndex = cursorTodoList.getColumnIndexOrThrow(TodoListDBContract.Direcciones.POSTAL_ID);

                //read data and add to ArrayList
                while (cursorTodoList.moveToNext()) {
                    Direccion Direccion = new Direccion(cursorTodoList.getInt(_idIndex),
                            cursorTodoList.getString(nombreIndex),
                            cursorTodoList.getInt(numeroIndex),
                            cursorTodoList.getInt(pisoIndex),
                            cursorTodoList.getString(puertaIndex),
                            cursorTodoList.getString(tipoIndex),
                            cursorTodoList.getInt(clienteIndex),
                            cursorTodoList.getInt(postalIndex));
                    DireccionList.add(Direccion);
                    System.out.println("\t\t" + cursorTodoList.getInt(_idIndex));
                }
                System.out.println("\t\t\t\t\t\t--------->" + DireccionList.size());
                // close cursor to free resources
                cursorTodoList.close();
            }
        }
        return DireccionList;
    }


}

package grup4.projecte.integrador.batoilogicdeliveries.Activities;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;

import grup4.projecte.integrador.batoilogicdeliveries.Activities.ui.home.HomeFragment;
import grup4.projecte.integrador.batoilogicdeliveries.Modelos.Albaran;
import grup4.projecte.integrador.batoilogicdeliveries.R;
import grup4.projecte.integrador.batoilogicdeliveries.database_Local.TodoListDBManager;

public class MyAdapter extends RecyclerView.Adapter<MyAdapter.MyViewHolder> {

    //    private final TodoListDBManager todoListDBManager;
    private ArrayList<Albaran> myDataSet;


    public interface OnItemClickListener {
        void onItemClick(Albaran item);
    }

    public interface OnLongItemClickListener {
        boolean onLongItemClick(Albaran item);
    }

    private OnItemClickListener listener;
    private OnLongItemClickListener longListener;


    static class MyViewHolder extends RecyclerView.ViewHolder {

        TextView tvCiudad;
        TextView tvNombreCliente;
        TextView tvDireccion;
        TextView tvIdPedido;
        CardView card;

        public MyViewHolder(View view) {

            super(view);
            this.tvCiudad = view.findViewById(R.id.tvCiudad);
            this.tvNombreCliente = view.findViewById(R.id.tvNombreCliente);

            this.tvDireccion = view.findViewById(R.id.tvDireccion);
            this.tvIdPedido = view.findViewById(R.id.tvIdPedido);
            this.card = view.findViewById(R.id.card);
        }

        public void bind(Albaran data, OnItemClickListener listener, OnLongItemClickListener longListener) {

            this.tvCiudad.setText(data.getPedido().getCiudad().getNombre());
            this.tvNombreCliente.setText(data.getPedido().getCliente().getNombre() + " " + data.getPedido().getCliente().getApellidos());
            this.tvDireccion.setText(data.getPedido().getDireccion().getTipo() + " " + data.getPedido().getDireccion().getNombre() + " N" + data.getPedido().getDireccion().getNumero() + " " + data.getPedido().getDireccion().getPiso() + "" + data.getPedido().getDireccion().getPuerta());
            this.tvIdPedido.setText(data.getPedido().getId() + "");

            this.card.setOnClickListener(v -> listener.onItemClick(data));
            this.card.setOnLongClickListener(v -> longListener.onLongItemClick(data));
        }

    }

    public MyAdapter(ArrayList<Albaran> myDataSet, OnItemClickListener listener, OnLongItemClickListener longListener) {
        this.listener = listener;
        this.longListener = longListener;
        this.myDataSet = myDataSet;
    }




    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_card_layout, parent, false);

        return new MyViewHolder(v);
    }


    @Override
    public void onBindViewHolder(@NonNull MyViewHolder viewHolder, int position) {
        viewHolder.bind(myDataSet.get(position), listener, longListener);
    }

    @Override
    public int getItemCount() {
        return myDataSet.size();
    }
}

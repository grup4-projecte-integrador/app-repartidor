package grup4.projecte.integrador.batoilogicdeliveries.database_Local;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import java.util.ArrayList;

import grup4.projecte.integrador.batoilogicdeliveries.Modelos_Internos.Pedido;


public class TodoListDBManager_Pedido {
    private TodoListDBHelper todoListDBHelper;

    public TodoListDBManager_Pedido(Context context) {
        todoListDBHelper = TodoListDBHelper.getInstance(context);
    }

    // Operations

    // CREATE new ros
    public void insertPedido(Pedido pedido) {
        // open database to read and write
        SQLiteDatabase sqLiteDatabase = todoListDBHelper.getWritableDatabase();
        int id;
        int cliente = pedido.getCliente();
        int direccion = pedido.getDireccion();
        Double precioTotal = pedido.getPrecioTotal();
        String observaciones = pedido.getObservaciones();
        String estado = pedido.getEstado();
        String fechaPedido = pedido.getFechaPedido();
        String fechaEstimada = pedido.getFechaEstimada();
        if (sqLiteDatabase != null) {
            ContentValues contentValue = new ContentValues();

            contentValue.put(TodoListDBContract.Pedidos.CLIENTE_ID, cliente);
            contentValue.put(TodoListDBContract.Pedidos.DIRECCION_ID, direccion);
            contentValue.put(TodoListDBContract.Pedidos.PRECIO_TOTAL, precioTotal);
            contentValue.put(TodoListDBContract.Pedidos.OBSERVACION, observaciones);
            contentValue.put(TodoListDBContract.Pedidos.ESTADO, estado);
            contentValue.put(TodoListDBContract.Pedidos.FECHAPEDIDO, fechaPedido);
            contentValue.put(TodoListDBContract.Pedidos.FECHAESTIMADA, fechaEstimada);


            sqLiteDatabase.insert(TodoListDBContract.Pedidos.TABLE_NAME, null, contentValue);

            // getWritableDatabase & getReadableDatabase() are expensive, you should leave your database connection
            // open for as long as you possibly need to access it
            //
            // sqLiteDatabase.close();
        }
    }


    // Get all data from Pedidos table
    public ArrayList<Pedido> getPedidos() {
        ArrayList<Pedido> PedidoList = new ArrayList<>();

        // open database to read
        SQLiteDatabase sqLiteDatabase = todoListDBHelper.getReadableDatabase();

        if (sqLiteDatabase != null) {
            String[] projection = new String[]{TodoListDBContract.Pedidos._ID,
                    TodoListDBContract.Pedidos.DIRECCION_ID,
                    TodoListDBContract.Pedidos.CLIENTE_ID,
                    TodoListDBContract.Pedidos.PRECIO_TOTAL,
                    TodoListDBContract.Pedidos.OBSERVACION,
                    TodoListDBContract.Pedidos.ESTADO,
                    TodoListDBContract.Pedidos.FECHAPEDIDO,
                    TodoListDBContract.Pedidos.FECHAESTIMADA
            };


            Cursor cursorTodoList = sqLiteDatabase.query(TodoListDBContract.Pedidos.TABLE_NAME,
                    projection,                     // The columns toView return
                    null,                  // no WHERE clause
                    null,               // no values for the WHERE clause
                    null,                   // don't group hte rows
                    null,                    // don't filter by row groups
                    null);                  // don't sort rows


            if (cursorTodoList != null) {
                // get the column indexes for required columns
                int _idIndex = cursorTodoList.getColumnIndexOrThrow(TodoListDBContract.Pedidos._ID);
                int clienteIndex = cursorTodoList.getColumnIndexOrThrow(TodoListDBContract.Pedidos.CLIENTE_ID);
                int direccionIndex = cursorTodoList.getColumnIndexOrThrow(TodoListDBContract.Pedidos.DIRECCION_ID);
                int totalIndex = cursorTodoList.getColumnIndexOrThrow(TodoListDBContract.Pedidos.PRECIO_TOTAL);
                int observacionIndex = cursorTodoList.getColumnIndexOrThrow(TodoListDBContract.Pedidos.OBSERVACION);
                int estadoIndex = cursorTodoList.getColumnIndexOrThrow(TodoListDBContract.Pedidos.ESTADO);
                int fechapedidoIndex = cursorTodoList.getColumnIndexOrThrow(TodoListDBContract.Pedidos.FECHAPEDIDO);
                int fechaestimadaIndex = cursorTodoList.getColumnIndexOrThrow(TodoListDBContract.Pedidos.FECHAESTIMADA);

                //read data and add to ArrayList
                while (cursorTodoList.moveToNext()) {
                    Pedido Pedido = new Pedido(cursorTodoList.getInt(_idIndex),
                            cursorTodoList.getInt(clienteIndex),
                            cursorTodoList.getInt(direccionIndex),
                            cursorTodoList.getDouble(totalIndex),
                            cursorTodoList.getString(observacionIndex),
                            cursorTodoList.getString(estadoIndex),
                            cursorTodoList.getString(fechapedidoIndex),
                            cursorTodoList.getString(fechaestimadaIndex)
                    );

                    PedidoList.add(Pedido);
                }
                // close cursor to free resources
                cursorTodoList.close();
            }
        }
        return PedidoList;
    }

    public void close() {
        todoListDBHelper.close();       // closes any opened database object
    }

    /**
     * Aci actualitzem el contingut de cada element al obrir el EditTaskActivity
     */
    public void update(Pedido pedido) {
        SQLiteDatabase sqLiteDatabase = todoListDBHelper.getWritableDatabase();
        int id = pedido.getId();
        int cliente = pedido.getCliente();
        int direccion = pedido.getDireccion();
        Double precioTotal = pedido.getPrecioTotal();
        String observaciones = pedido.getObservaciones();
        String estado = pedido.getEstado();
        String fechaPedido = pedido.getFechaPedido();
        String fechaEstimada = pedido.getFechaEstimada();
        if (sqLiteDatabase != null) {
            ContentValues contentValue = new ContentValues();

            contentValue.put(TodoListDBContract.Pedidos.CLIENTE_ID, cliente);
            contentValue.put(TodoListDBContract.Pedidos.DIRECCION_ID, direccion);
            contentValue.put(TodoListDBContract.Pedidos.PRECIO_TOTAL, precioTotal);
            contentValue.put(TodoListDBContract.Pedidos.OBSERVACION, observaciones);
            contentValue.put(TodoListDBContract.Pedidos.ESTADO, estado);
            contentValue.put(TodoListDBContract.Pedidos.FECHAPEDIDO, fechaPedido);
            contentValue.put(TodoListDBContract.Pedidos.FECHAESTIMADA, fechaEstimada);


            sqLiteDatabase.update(TodoListDBContract.Pedidos.TABLE_NAME, contentValue, "_id=" + id, null);


        }
    }

    public void deletePedidos(int id) {
        SQLiteDatabase sqLiteDatabase = todoListDBHelper.getWritableDatabase();
        if (sqLiteDatabase != null) {
            sqLiteDatabase.delete(TodoListDBContract.Pedidos.TABLE_NAME, "_id=" + id, null);

        }

    }

    public void deleteComplPedidos() {
        SQLiteDatabase sqLiteDatabase = todoListDBHelper.getWritableDatabase();
        String stat = "Completed";
        if (sqLiteDatabase != null) {
//            sqLiteDatabase.delete(TodoListDBContract.Pedidos.TABLE_NAME, TodoListDBContract.Pedidos.STATUS + "=" + stat, null);
            sqLiteDatabase.execSQL("DELETE FROM " + TodoListDBContract.Pedidos.TABLE_NAME + " WHERE status='Completed'");
        }
    }

    public void deleteAllPedidos() {
        SQLiteDatabase sqLiteDatabase = todoListDBHelper.getWritableDatabase();
        if (sqLiteDatabase != null) {
            sqLiteDatabase.delete(TodoListDBContract.Pedidos.TABLE_NAME, null, null);

        }

    }


    public ArrayList<Pedido> getSelection(String stat) {
        ArrayList<Pedido> PedidoList = new ArrayList<>();
        String[] tipus = new String[]{stat};
        System.out.println("\t\t\t---------------->" + stat);

        // open database to read
        SQLiteDatabase sqLiteDatabase = todoListDBHelper.getReadableDatabase();

        if (sqLiteDatabase != null) {
            String[] projection = new String[]{TodoListDBContract.Pedidos._ID,
                    TodoListDBContract.Pedidos.DIRECCION_ID,
                    TodoListDBContract.Pedidos.CLIENTE_ID,
                    TodoListDBContract.Pedidos.PRECIO_TOTAL,
                    TodoListDBContract.Pedidos.OBSERVACION,
                    TodoListDBContract.Pedidos.ESTADO,
                    TodoListDBContract.Pedidos.FECHAPEDIDO,
                    TodoListDBContract.Pedidos.FECHAESTIMADA
            };
//
//            taskList.clear();
            System.out.println(PedidoList.size());
//
            Cursor cursorTodoList = sqLiteDatabase.query(TodoListDBContract.Pedidos.TABLE_NAME,
                    projection,                     // The columns toView return
                    "status=?",                  // no WHERE clause
                    tipus,               // no values for the WHERE clause
                    null,                   // don't group hte rows
                    null,                    // don't filter by row groups
                    null);                  // don't sort rows


            if (cursorTodoList != null) {
                // get the column indexes for required columns
                int _idIndex = cursorTodoList.getColumnIndexOrThrow(TodoListDBContract.Pedidos._ID);
                int clienteIndex = cursorTodoList.getColumnIndexOrThrow(TodoListDBContract.Pedidos.CLIENTE_ID);
                int direccionIndex = cursorTodoList.getColumnIndexOrThrow(TodoListDBContract.Pedidos.DIRECCION_ID);
                int totalIndex = cursorTodoList.getColumnIndexOrThrow(TodoListDBContract.Pedidos.PRECIO_TOTAL);
                int observacionIndex = cursorTodoList.getColumnIndexOrThrow(TodoListDBContract.Pedidos.OBSERVACION);
                int estadoIndex = cursorTodoList.getColumnIndexOrThrow(TodoListDBContract.Pedidos.ESTADO);
                int fechapedidoIndex = cursorTodoList.getColumnIndexOrThrow(TodoListDBContract.Pedidos.FECHAPEDIDO);
                int fechaestimadaIndex = cursorTodoList.getColumnIndexOrThrow(TodoListDBContract.Pedidos.FECHAESTIMADA);

                //read data and add to ArrayList
                while (cursorTodoList.moveToNext()) {
                    Pedido Pedido = new Pedido(cursorTodoList.getInt(_idIndex),
                            cursorTodoList.getInt(clienteIndex),
                            cursorTodoList.getInt(direccionIndex),
                            cursorTodoList.getDouble(totalIndex),
                            cursorTodoList.getString(observacionIndex),
                            cursorTodoList.getString(estadoIndex),
                            cursorTodoList.getString(fechapedidoIndex),
                            cursorTodoList.getString(fechaestimadaIndex));
                    PedidoList.add(Pedido);
                    System.out.println("\t\t" + cursorTodoList.getInt(_idIndex));
                }
                System.out.println("\t\t\t\t\t\t--------->" + PedidoList.size());
                // close cursor to free resources
                cursorTodoList.close();
            }
        }
        return PedidoList;
    }


}

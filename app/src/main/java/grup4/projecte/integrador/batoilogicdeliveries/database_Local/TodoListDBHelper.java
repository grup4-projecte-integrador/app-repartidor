package grup4.projecte.integrador.batoilogicdeliveries.database_Local;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class TodoListDBHelper extends SQLiteOpenHelper {

    // instance to SQLiteOpenHelper
    private static TodoListDBHelper instanceDBHelper;


    // This method assures only one instance of TodoListDBHelper for all the application.
    // Use the application context, to not leak Activity context

    public static synchronized TodoListDBHelper getInstance(Context context) {
        // instance must be unique
        if (instanceDBHelper == null) {
            instanceDBHelper = new TodoListDBHelper(context.getApplicationContext());
        }
        return instanceDBHelper;
    }

    // Constructor should be private to prevent direct instatiation
    private TodoListDBHelper(Context context) {
        super(context, TodoListDBContract.DB_NAME, null, TodoListDBContract.DB_VERSION);
    }


    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {
        sqLiteDatabase.execSQL(TodoListDBContract.Albarans.CREATE_TABLE);
        sqLiteDatabase.execSQL(TodoListDBContract.Pedidos.CREATE_TABLE);
        sqLiteDatabase.execSQL(TodoListDBContract.Clientes.CREATE_TABLE);
        sqLiteDatabase.execSQL(TodoListDBContract.Rutas.CREATE_TABLE);
        sqLiteDatabase.execSQL(TodoListDBContract.Ciudades.CREATE_TABLE);
        sqLiteDatabase.execSQL(TodoListDBContract.Codigo_Postal.CREATE_TABLE);
        sqLiteDatabase.execSQL(TodoListDBContract.Direcciones.CREATE_TABLE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int oldVersion, int newVersion) {
        // the upgrade policy is simply discard the table and start over
        sqLiteDatabase.execSQL(TodoListDBContract.Albarans.DELETE_TABLE);
        sqLiteDatabase.execSQL(TodoListDBContract.Pedidos.DELETE_TABLE);
        sqLiteDatabase.execSQL(TodoListDBContract.Clientes.DELETE_TABLE);
        sqLiteDatabase.execSQL(TodoListDBContract.Rutas.DELETE_TABLE);
        sqLiteDatabase.execSQL(TodoListDBContract.Ciudades.DELETE_TABLE);
        sqLiteDatabase.execSQL(TodoListDBContract.Codigo_Postal.DELETE_TABLE);
        sqLiteDatabase.execSQL(TodoListDBContract.Direcciones.DELETE_TABLE);
        // create again the DB
        onCreate(sqLiteDatabase);
    }
}

package grup4.projecte.integrador.batoilogicdeliveries.Modelos;

import java.io.Serializable;

public class Ruta implements Serializable {

    public int id;
    public Repartidor repartidor;


    public Ruta() {
    }

    public Ruta(int id, Repartidor repartidor) {
        this.id = id;
        this.repartidor = repartidor;
    }

    public Ruta(Ruta ruta) {
        this.id = ruta.getId();
        this.repartidor = ruta.getRepartidor();

    }


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Repartidor getRepartidor() {
        return repartidor;
    }

    public void setRepartidor(Repartidor repartidor) {
        this.repartidor = repartidor;
    }
}

package grup4.projecte.integrador.batoilogicdeliveries.Activities;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;
import androidx.navigation.ui.AppBarConfiguration;
import androidx.navigation.ui.NavigationUI;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.Manifest;
import android.app.job.JobInfo;
import android.app.job.JobScheduler;
import android.content.ComponentName;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.navigation.NavigationView;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.concurrent.TimeUnit;

import grup4.projecte.integrador.batoilogicdeliveries.Modelos.Albaran;
import grup4.projecte.integrador.batoilogicdeliveries.Modelos.Ciudad;
import grup4.projecte.integrador.batoilogicdeliveries.Modelos.Cliente;
import grup4.projecte.integrador.batoilogicdeliveries.Modelos.Direccion;
import grup4.projecte.integrador.batoilogicdeliveries.Modelos.Pedido;
import grup4.projecte.integrador.batoilogicdeliveries.Modelos.Ruta;
import grup4.projecte.integrador.batoilogicdeliveries.R;

import static androidx.core.content.ContextCompat.getSystemService;

public class AlbaranActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener {

    private AppBarConfiguration mAppBarConfiguration;


    private static final int LOCATION_REQUEST = 1337 + 3;
    private static final String[] INITIAL_PERMS = {
            Manifest.permission.ACCESS_FINE_LOCATION,
            Manifest.permission.READ_CONTACTS
    };


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_navigation_drawer);

        setUI();
        if (!canAccessLocation()) {
            requestPermissions(INITIAL_PERMS, LOCATION_REQUEST);
        }
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            doService();
        }
    }

    public void setUI() {


        FloatingActionButton fab = findViewById(R.id.fabMap);


        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);


        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        NavigationView navigationView = findViewById(R.id.nav_view);

        mAppBarConfiguration = new AppBarConfiguration.Builder(
                R.id.nav_home)
                .setDrawerLayout(drawer)
                .build();


        NavController navController = Navigation.findNavController(this, R.id.nav_host_fragment);
        NavigationUI.setupActionBarWithNavController(this, navController, mAppBarConfiguration);
        NavigationUI.setupWithNavController(navigationView, navController);

        asignar();

    }


    private boolean canAccessLocation() {
        return (hasPermission(Manifest.permission.ACCESS_FINE_LOCATION));
    }

    private boolean hasPermission(String perm) {
        return (PackageManager.PERMISSION_GRANTED == checkSelfPermission(perm));
    }


    private void asignar() {
        NavigationView navigationView = findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
    }


    @Override
    public boolean onSupportNavigateUp() {
        NavController navController = Navigation.findNavController(this, R.id.nav_host_fragment);
        return NavigationUI.navigateUp(navController, mAppBarConfiguration)
                || super.onSupportNavigateUp();
    }


    @Override

    public boolean onNavigationItemSelected(MenuItem item) {

        Intent i;
        switch (item.getItemId()) {

            case R.id.nav_home:

                i = new Intent(AlbaranActivity.this, LoginActivity.class);
                startActivity(i);
                cacelJob();
                finish();
                return true;

        }

        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);

        return true;
    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    private void doService() {
        JobScheduler jobScheduler = (JobScheduler) getSystemService(JOB_SCHEDULER_SERVICE);
        JobInfo.Builder builder = new JobInfo.Builder(777, new ComponentName(this, GetUbicacion.class));  //Specify which JobService performs the operation
        builder.setMinimumLatency(TimeUnit.MILLISECONDS.toMillis(10)); //Minimum latency of execution
        builder.setOverrideDeadline(TimeUnit.MILLISECONDS.toMillis(15));  //Maximum latency of execution
        builder.setRequiredNetworkType(JobInfo.NETWORK_TYPE_NOT_ROAMING);  //Non-roaming network state
        builder.setBackoffCriteria(TimeUnit.MILLISECONDS.toMillis(10), JobInfo.BACKOFF_POLICY_LINEAR);  //Linear Retry Scheme
        builder.setRequiresCharging(false); // Uncharged state
        int resultCode = jobScheduler.schedule(builder.build());

        if(resultCode == JobScheduler.RESULT_SUCCESS)
            Log.d("UBICACION","JOB SCHEDULED");
        else
            Log.d("UBICACION","JOB SCHEDULED FAILED");
    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    private void cacelJob() {
        JobScheduler scheduler = (JobScheduler) getSystemService(JOB_SCHEDULER_SERVICE);
        scheduler.cancel(777);
        Log.d("UBICACION","Ubicacion cancelled");
    }
}

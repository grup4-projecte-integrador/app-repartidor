package grup4.projecte.integrador.batoilogicdeliveries.Modelos;

import java.io.Serializable;

public class Repartidor implements Serializable {

    public int id;
    public String nombre;
    public String Apellidos;
    public String dni;
    public String contrasenya;
    public Camion Camion;
    public int camionID;

    public Repartidor() {
    }

    public Repartidor(String nombre, String apellidos, String dni, String contrasenya, grup4.projecte.integrador.batoilogicdeliveries.Modelos.Camion camion) {
        this.nombre = nombre;
        Apellidos = apellidos;
        this.dni = dni;
        this.contrasenya = contrasenya;
        Camion = camion;
    }

    public Repartidor(Repartidor repartidor) {
        this.id = repartidor.getId();
        this.dni =repartidor.getDni();
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getApellidos() {
        return Apellidos;
    }

    public void setApellidos(String apellidos) {
        Apellidos = apellidos;
    }

    public String getDni() {
        return dni;
    }

    public void setDni(String dni) {
        this.dni = dni;
    }

    public String getContrasenya() {
        return contrasenya;
    }

    public void setContrasenya(String contrasenya) {
        this.contrasenya = contrasenya;
    }

    public grup4.projecte.integrador.batoilogicdeliveries.Modelos.Camion getCamion() {
        return Camion;
    }

    public void setCamion(grup4.projecte.integrador.batoilogicdeliveries.Modelos.Camion camion) {
        Camion = camion;
    }


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getCamionID() {
        return camionID;
    }

    public void setCamionID(int camionID) {
        this.camionID = camionID;
    }
}

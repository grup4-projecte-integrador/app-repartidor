package grup4.projecte.integrador.batoilogicdeliveries.Modelos;

import java.io.Serializable;

public class Pedido implements Serializable {

    public int id;
    public Cliente cliente;
    public Direccion direccion;
    public Ciudad ciudad;
    public Double precioTotal;
    public String descripcion;
    public String observaciones;
    public String estado;
    public String fechaPedido;
    public String fechaEstimada;


    public Pedido(int id, Cliente cliente, Direccion direccion, Ciudad ciudad, Double precioTotal, String observaciones, String estado, String fechaPedido, String fechaEstimada) {
        this.id = id;
        this.cliente = cliente;
        this.direccion = direccion;
        this.ciudad = ciudad;
        this.precioTotal = precioTotal;
        this.fechaPedido = fechaPedido;
        this.fechaEstimada= fechaEstimada;
        this.observaciones = observaciones;
        this.estado = estado;
    }

    public Pedido() {

    }

    public String getFechaPedido() {
        return fechaPedido;
    }

    public void setFechaPedido(String fechaPedido) {
        this.fechaPedido = fechaPedido;
    }

    public String getFechaEstimada() {
        return fechaEstimada;
    }

    public void setFechaEstimada(String fechaEstimada) {
        this.fechaEstimada = fechaEstimada;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Cliente getCliente() {
        return cliente;
    }

    public void setCliente(Cliente cliente) {
        this.cliente = cliente;
    }

    public Direccion getDireccion() {
        return direccion;
    }

    public void setDireccion(Direccion direccion) {
        this.direccion = direccion;
    }

    public Ciudad getCiudad() {
        return ciudad;
    }

    public void setCiudad(Ciudad ciudad) {
        this.ciudad = ciudad;
    }

    public Double getPrecioTotal() {
        return precioTotal;
    }

    public void setPrecioTotal(Double precioTotal) {
        this.precioTotal = precioTotal;
    }


    public String getObservaciones() {
        return observaciones;
    }

    public void setObservaciones(String observaciones) {
        this.observaciones = observaciones;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }


}

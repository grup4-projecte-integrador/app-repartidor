package grup4.projecte.integrador.batoilogicdeliveries.database_Local;

/**
 * Class that defines TodoListDB schema
 */

public final class TodoListDBContract {
    // Common fields to all DB

    // Database name
    public static final String DB_NAME = "BATOILOGIC.DB";
    // Database Version
    public static final int DB_VERSION = 1;

    // To prevent someone from accidentally instantiating the contract class:
    // make the constructor private.
    private TodoListDBContract() {

    }

    // schema

    // TABLE Albarans: Inner class that defines the table Albarans contents
    public static class Albarans {
        // Table name
        public static final String TABLE_NAME = "Albarans";

        // Columns names
        public static final String _ID = "_id";
        public static final String PEDIDO_ID = "pedido_id";
        public static final String RUTA_ID = "ruta_id";

        // CREATE_TABLE SQL String
        public static final String CREATE_TABLE = "CREATE TABLE " + Albarans.TABLE_NAME
                + " ("
                + Albarans._ID + " INTEGER PRIMARY KEY AUTOINCREMENT, "
                + Albarans.PEDIDO_ID + " INTEGER, "
                + Albarans.RUTA_ID + " INTEGER, "
                + "FOREIGN KEY(" + Albarans.PEDIDO_ID + ") references " + Pedidos.TABLE_NAME + "(" + Pedidos._ID + "), "
                + "FOREIGN KEY(" + Albarans.RUTA_ID + ") references " + Rutas.TABLE_NAME + "(" + Rutas._ID + ") "
                + ");";

        public static final String DELETE_TABLE = "DROP TABLE IF EXISTS " + Albarans.TABLE_NAME;
    }


    // TABLE Albarans: Inner class that defines the table Albarans contents
    public static class Pedidos {
        // Table name
        public static final String TABLE_NAME = "Pedidos";

        // Columns names
        public static final String _ID = "_id";
        public static final String PRECIO_TOTAL = "preciototal";
        public static final String OBSERVACION = "observacion";
        public static final String FECHAPEDIDO = "fechapedido";
        public static final String FECHAESTIMADA = "fechaestimada";
        public static final String ESTADO = "estado";
        public static final String CLIENTE_ID = "cliente_id";
        public static final String DIRECCION_ID = "direccion_id";

        // CREATE_TABLE SQL String
        public static final String CREATE_TABLE = "CREATE TABLE " + Pedidos.TABLE_NAME
                + " ("
                + Pedidos._ID + " INTEGER PRIMARY KEY AUTOINCREMENT, "
                + Pedidos.PRECIO_TOTAL + " FLOAT, "
                + Pedidos.OBSERVACION + " TEXT, "
                + Pedidos.FECHAPEDIDO + " TEXT, "
                + Pedidos.FECHAESTIMADA + " TEXT, "
                + Pedidos.ESTADO + " TEXT, "
                + Pedidos.CLIENTE_ID + " INTEGER, "
                + Pedidos.DIRECCION_ID + " INTEGER, "
                + "FOREIGN KEY(" + Pedidos.CLIENTE_ID + ") references " + Clientes.TABLE_NAME + "(" + Clientes._ID + "), "
                + "FOREIGN KEY(" + Pedidos.DIRECCION_ID + ") references " + Direcciones.TABLE_NAME + "(" + Direcciones._ID + ") "
                + ");";

        public static final String DELETE_TABLE = "DROP TABLE IF EXISTS " + Pedidos.TABLE_NAME;
    }


    // TABLE Cliente: Inner class that defines the table Clientes contents
    public static class Clientes {
        // Table name
        public static final String TABLE_NAME = "Clientes";

        // Columns names
        public static final String _ID = "_id";
        public static final String NOMBRE = "nombre";
        public static final String APELLIDOS = "apellidos";
        public static final String FECHANACIMIENTO = "fechanacimiento";


        // CREATE_TABLE SQL String
        public static final String CREATE_TABLE = "CREATE TABLE " + Clientes.TABLE_NAME
                + " ("
                + Clientes._ID + " INTEGER PRIMARY KEY AUTOINCREMENT, "
                + Clientes.NOMBRE + " TEXT, "
                + Clientes.APELLIDOS + " TEXT, "
                + Clientes.FECHANACIMIENTO + " TEXT "
                + ");";

        public static final String DELETE_TABLE = "DROP TABLE IF EXISTS " + Clientes.TABLE_NAME;
    }


    // TABLE Rutas: Inner class that defines the table Rutas contents
    public static class Rutas {
        // Table name
        public static final String TABLE_NAME = "Rutas";

        // Columns names
        public static final String _ID = "_id";

        public static final String REPARTIDOR_ID = "repartidor_id";


        // CREATE_TABLE SQL String
        public static final String CREATE_TABLE = "CREATE TABLE " + Rutas.TABLE_NAME
                + " ("
                + Rutas._ID + " INTEGER PRIMARY KEY AUTOINCREMENT, "
                + Rutas.REPARTIDOR_ID + " INTEGER "
                + ");";

        public static final String DELETE_TABLE = "DROP TABLE IF EXISTS " + Rutas.TABLE_NAME;
    }


    // TABLE Rutas: Inner class that defines the table Rutas contents
    public static class Ciudades {
        // Table name
        public static final String TABLE_NAME = "Ciudades";

        // Columns names
        public static final String _ID = "_id";
        public static final String NOMBRE = "nombre";
        public static final String PROVINCIA = "provincia";


        // CREATE_TABLE SQL String
        public static final String CREATE_TABLE = "CREATE TABLE " + Ciudades.TABLE_NAME
                + " ("
                + Ciudades._ID + " INTEGER PRIMARY KEY AUTOINCREMENT, "
                + Ciudades.NOMBRE + " TEXT, "
                + Ciudades.PROVINCIA + " TEXT "
                + ");";

        public static final String DELETE_TABLE = "DROP TABLE IF EXISTS " + Ciudades.TABLE_NAME;
    }

    // TABLE Codigo_Postal: Inner class that defines the table Codigo_Postal contents
    public static class Codigo_Postal {
        // Table name
        public static final String TABLE_NAME = "Codigo_Postal";

        // Columns names
        public static final String _ID = "_id";
        public static final String NUMERO = "numero";
        public static final String CIUDAD_ID = "ciudad";


        // CREATE_TABLE SQL String
        public static final String CREATE_TABLE = "CREATE TABLE " + Codigo_Postal.TABLE_NAME
                + " ("
                + Codigo_Postal._ID + " INTEGER PRIMARY KEY AUTOINCREMENT, "
                + Codigo_Postal.NUMERO + " TEXT, "
                + Codigo_Postal.CIUDAD_ID + " INTEGER, "
                + "FOREIGN KEY(" + Codigo_Postal.CIUDAD_ID + ") references " + Ciudades.TABLE_NAME + "(" + Ciudades._ID + ") "
                + ");";

        public static final String DELETE_TABLE = "DROP TABLE IF EXISTS " + Codigo_Postal.TABLE_NAME;
    }


    // TABLE Direcciones: Inner class that defines the table Direcciones contents
    public static class Direcciones {
        // Table name
        public static final String TABLE_NAME = "Direcciones";

        // Columns names
        public static final String _ID = "_id";
        public static final String NOMBRE = "nombre";
        public static final String TIPO = "tipo";
        public static final String NUMERO = "numero";
        public static final String PISO = "piso";
        public static final String PUERTA = "puerta";
        public static final String CLIENTE_ID = "cliente_id";
        public static final String POSTAL_ID = "postal_id";


        // CREATE_TABLE SQL String
        public static final String CREATE_TABLE = "CREATE TABLE " + Direcciones.TABLE_NAME
                + " ("
                + Direcciones._ID + " INTEGER PRIMARY KEY AUTOINCREMENT, "
                + Direcciones.NOMBRE + " TEXT,"
                + Direcciones.TIPO + " TEXT, "
                + Direcciones.NUMERO + " INTEGER, "
                + Direcciones.PISO + " INTEGER, "
                + Direcciones.PUERTA + " TEXT, "
                + Direcciones.CLIENTE_ID + " INTEGER, "
                + Direcciones.POSTAL_ID + " INTEGER, "
                + "FOREIGN KEY(" + Direcciones.CLIENTE_ID + ") references " + Clientes.TABLE_NAME + "(" + Clientes._ID + "), "
                + "FOREIGN KEY(" + Direcciones.POSTAL_ID + ") references " + Codigo_Postal.TABLE_NAME + "(" + Codigo_Postal._ID + ") "
                + ");";

        public static final String DELETE_TABLE = "DROP TABLE IF EXISTS " + Direcciones.TABLE_NAME;
    }

}

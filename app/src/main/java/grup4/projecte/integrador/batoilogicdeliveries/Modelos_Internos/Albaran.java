package grup4.projecte.integrador.batoilogicdeliveries.Modelos_Internos;


import java.io.Serializable;

public class Albaran implements Serializable {

    public int id;
    public int pedido;
    public int ruta;

    public Albaran(int id, int pedido, int ruta) {
        this.id = id;
        this.pedido = pedido;
        this.ruta = ruta;
    }

    public Albaran() {

    }


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getPedido() {
        return pedido;
    }

    public void setPedido(int pedido) {
        this.pedido = pedido;
    }

    public int getRuta() {
        return ruta;
    }

    public void setRuta(int ruta) {
        this.ruta = ruta;
    }


}

package grup4.projecte.integrador.batoilogicdeliveries.Activities;

import android.Manifest;
import android.app.job.JobInfo;
import android.app.job.JobParameters;
import android.app.job.JobScheduler;
import android.app.job.JobService;
import android.content.ComponentName;
import android.content.Context;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationManager;
import android.os.Build;
import android.util.Log;

import androidx.annotation.RequiresApi;
import androidx.core.app.ActivityCompat;

import java.io.BufferedOutputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.Socket;
import java.net.SocketException;
import java.net.UnknownHostException;
import java.util.concurrent.TimeUnit;

import grup4.projecte.integrador.batoilogicdeliveries.Modelos.Repartidor;
import grup4.projecte.integrador.batoilogicdeliveries.Modelos.Repartidor_Actual;

@RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
public class GetUbicacion extends JobService {
    private static final String SERVER = "grup4.cipfpbatoi.es";
    private static final int PORT = 9090;
    private LocationManager locationManager;
    private Repartidor r;
    private Repartidor_Actual rAct;

    @Override
    public boolean onStartJob(JobParameters params) {
        Log.d("UBICACION", "JobStarted");
        doService();
        doJob(params);
        return true;
    }

    @Override
    public boolean onStopJob(JobParameters params) {
        Log.d("UBICACION", "Job cancelled before completion");
        return true;
    }

    private void doJob(final JobParameters parameters) {
        new Thread(new Runnable() {

            @Override
            public void run() {
                Log.d("UBICACION", "JobStarted do job");
                System.out.println("WELCOME TO UDP CLIENT");

                try (Socket clientSocket = new Socket(SERVER, PORT); DataOutputStream outputStreamToServer = new DataOutputStream(new BufferedOutputStream(clientSocket.getOutputStream()));) {
                    System.out.println("TRYING TO CONNECT TO SERVER..." + "[" + SERVER + "," + PORT + "]");
                    System.out.println("..............CONNECTION TO SERVER ESTABLISHED ON LOCAL PORT: "
                            + clientSocket.getLocalPort());
                    rAct = new Repartidor_Actual();
                    r = rAct.getRepartidor();

                    outputStreamToServer.writeUTF(getUbi());
                    outputStreamToServer.flush();
                    System.out.println("End of stream.........");

                } catch (UnknownHostException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                }

                Log.d("UBICACION", "Job Finished");
                jobFinished(parameters, false);
            }
        }).start();
    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    private void doService() {
        JobScheduler jobScheduler = (JobScheduler) getSystemService(JOB_SCHEDULER_SERVICE);
        JobInfo.Builder builder = new JobInfo.Builder(777, new ComponentName(this, GetUbicacion.class));  //Specify which JobService performs the operation
        builder.setMinimumLatency(TimeUnit.MINUTES.toMillis(1)); //Minimum latency of execution
        builder.setOverrideDeadline(TimeUnit.MINUTES.toMillis(2));  //Maximum latency of execution
        builder.setRequiredNetworkType(JobInfo.NETWORK_TYPE_NOT_ROAMING);  //Non-roaming network state
        builder.setBackoffCriteria(TimeUnit.MINUTES.toMillis(1), JobInfo.BACKOFF_POLICY_LINEAR);  //Linear Retry Scheme
        builder.setRequiresCharging(false); // Uncharged state
        int resultCode = jobScheduler.schedule(builder.build());

        if (resultCode == JobScheduler.RESULT_SUCCESS)
            Log.d("UBICACION", "JOB SCHEDULED");
        else
            Log.d("UBICACION", "JOB SCHEDULED FAILED");
    }

    private String getUbi() {
        Location location = null;
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) {

            locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
            location = locationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);
        }
        int id = r.getId();

        //Info id latitud longitud
        Log.d("UBICACION", location.getLongitude() + " " + location.getLatitude());
        String info = id + "//" + location.getLongitude() + "//" + location.getLatitude();
        System.out.println(info);

        return info;
    }
}

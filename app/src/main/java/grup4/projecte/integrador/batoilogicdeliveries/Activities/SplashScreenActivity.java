package grup4.projecte.integrador.batoilogicdeliveries.Activities;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.app.AppCompatDelegate;

import android.content.Context;
import android.content.Intent;
import android.content.res.Configuration;
import android.os.Bundle;
import android.os.Handler;

import grup4.projecte.integrador.batoilogicdeliveries.R;

public class SplashScreenActivity extends AppCompatActivity {

    private int SPLASH_TIME_OUT = 3000;
    private Context context;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash_screen);

        context = this;
        Handler handler = new Handler();


        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                SplashScreenActivity.this.startActivity(new Intent(SplashScreenActivity.this.getApplicationContext(), LoginActivity.class));
                finish();
            }
        }, SPLASH_TIME_OUT);
    }
}

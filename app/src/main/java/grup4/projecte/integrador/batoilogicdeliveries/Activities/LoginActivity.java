package grup4.projecte.integrador.batoilogicdeliveries.Activities;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.app.AppCompatDelegate;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.os.Bundle;
import android.os.StrictMode;
import android.preference.PreferenceManager;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import org.apache.xmlrpc.XmlRpcException;

import java.sql.Connection;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import grup4.projecte.integrador.batoilogicdeliveries.Conexion.ConexionBD;
import grup4.projecte.integrador.batoilogicdeliveries.Modelos.Camion;
import grup4.projecte.integrador.batoilogicdeliveries.Modelos.Repartidor;
import grup4.projecte.integrador.batoilogicdeliveries.Modelos.Repartidor_Actual;
import grup4.projecte.integrador.batoilogicdeliveries.R;

import static java.util.Arrays.asList;

public class LoginActivity extends AppCompatActivity {

    public Button btEntrar;
    public EditText etUser;
    public EditText etPassword;
    public static Repartidor_Actual rActual;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);

        ConexionBD.client = ConexionBD.getInstance();

        setUI();
    }

    public Camion findCamion(int id) {

        Camion camion = null;
        try {
            List l = asList((Object[]) ConexionBD.client.execute("execute_kw", asList(
                    ConexionBD.DB, ConexionBD.uid, ConexionBD.PASS,
                    "batoilogic.camion", "search_read",
                    asList(asList(
                            asList("id", "=", id)
                    )), new HashMap() {
                        {
                            put("fields", asList("id", "capacidad"));
                        }
                    }
            )));

            HashMap hash = (HashMap) l.get(0);

            camion = new Camion();
            camion.setCantidad((Integer) hash.get("capacidad"));
            camion.setId((int) hash.get("id"));


        } catch (XmlRpcException e) {
            e.printStackTrace();
        }

        return camion;


    }


    public void setUI() {


        etUser = findViewById(R.id.etUser);

        etPassword = findViewById(R.id.etPassword);

        btEntrar = findViewById(R.id.btEntrar);
        rActual = new Repartidor_Actual();

        btEntrar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                try {
                    Repartidor repartidor = findByPK(etUser.getText().toString(), etPassword.getText().toString());

                    if (repartidor != null) {
                        repartidor.setCamion(findCamion(repartidor.getCamionID()));
                        startActivity(new Intent(getApplicationContext(), MainActivity.class));
                        rActual.setRepartidor(repartidor);
                        finish();

                    } else {

                        hideKeyboardFrom(getApplicationContext(), v);
                        Toast.makeText(getApplicationContext(), "Usuario no encontrado", Toast.LENGTH_LONG).show();
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });

    }

    public Repartidor findByPK(String dni, String password) throws Exception {

        Repartidor repartidorDevolver = null;
        ArrayList<Repartidor> repartidors = new ArrayList<>();
        List l = asList((Object[]) ConexionBD.client.execute("execute_kw", asList(
                ConexionBD.DB, ConexionBD.uid, ConexionBD.PASS,
                "batoilogic.repartidor", "search_read",
                asList(asList(

                )), new HashMap() {
                    {
                        put("fields", asList("dni", "password", "id", "camion_id"));
                    }
                }
        )));

        for (int i = 0; i < l.size(); i++) {

            HashMap hm = (HashMap) l.get(i);
            Repartidor r = new Repartidor();
            r.setId((Integer) hm.get("id"));
            r.setDni(hm.get("dni").toString());
            r.setContrasenya(hm.get("password").toString());
            Object[] aux = (Object[]) hm.get("camion_id");
            r.setCamionID((int) aux[0]);

            repartidors.add(r);
        }

        boolean semaforo = true;

        for (int i = 0; i < repartidors.size() && semaforo; i++) {

            if (repartidors.get(i).getContrasenya().equals(password) && repartidors.get(i).getDni().equals(dni)) {

                semaforo = false;
                repartidorDevolver = repartidors.get(i);


            }
        }

        if (repartidorDevolver != null) {
            List l2 = asList((Object[]) ConexionBD.client.execute("execute_kw", asList(
                    ConexionBD.DB, ConexionBD.uid, ConexionBD.PASS,
                    "batoilogic.camion", "search_read",
                    asList(asList(

                            asList("id", "=", repartidorDevolver.getCamionID())

                    )), new HashMap() {
                        {
                            put("fields", asList("id", "capacidad"));
                        }
                    }
            )));
            Camion c = new Camion();
            HashMap hmAux = (HashMap) l2.get(0);
            c.setId((int) hmAux.get("id"));
            c.setCantidad((int) hmAux.get("capacidad"));
            repartidorDevolver.setCamion(c);
            System.out.println("\t\t ID DEL REPARTIDOR " + repartidorDevolver.getId());
            System.out.println("\t\t--->Capacidad " + c.getCantidad());
            return repartidorDevolver;
        } else{
            return null;
        }


    }

    public static void hideKeyboardFrom(Context context, View view) {
        InputMethodManager imm = (InputMethodManager) context.getSystemService(Activity.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
    }
}

package grup4.projecte.integrador.batoilogicdeliveries.database_Local;

import android.content.Context;
import android.os.AsyncTask;
import android.view.View;

import androidx.recyclerview.widget.RecyclerView;

import org.apache.xmlrpc.XmlRpcException;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import grup4.projecte.integrador.batoilogicdeliveries.Conexion.ConexionBD;
import grup4.projecte.integrador.batoilogicdeliveries.Modelos.Repartidor;
import grup4.projecte.integrador.batoilogicdeliveries.Modelos.Repartidor_Actual;
import grup4.projecte.integrador.batoilogicdeliveries.Modelos_Internos.Albaran;
import grup4.projecte.integrador.batoilogicdeliveries.Modelos_Internos.Ciudad;
import grup4.projecte.integrador.batoilogicdeliveries.Modelos_Internos.Cliente;
import grup4.projecte.integrador.batoilogicdeliveries.Modelos_Internos.CodigoPostal;
import grup4.projecte.integrador.batoilogicdeliveries.Modelos_Internos.Direccion;
import grup4.projecte.integrador.batoilogicdeliveries.Modelos_Internos.Pedido;
import grup4.projecte.integrador.batoilogicdeliveries.Modelos_Internos.Ruta;

import static java.util.Arrays.asList;
import static java.util.Arrays.deepEquals;

public class Clonar_Local {

    Context context;
    ConsultaBD consultaBD;
    Repartidor_Actual rActual;
    Repartidor rA;
    int rep;

    public Clonar_Local(Context context) {
        this.context = context;
        rActual = new Repartidor_Actual();
        rA = new Repartidor(rActual.getRepartidor());

        consultaBD = new ConsultaBD();
        consultaBD.execute();
    }

    class ConsultaBD extends AsyncTask<Void, Void, ArrayList<Albaran>> {


        public ArrayList<Albaran> al = new ArrayList<>();


        public ConsultaBD() {


        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

        }

        @Override
        protected ArrayList<Albaran> doInBackground(Void... voids) {


            try {

//                al = afegirElements();
                afegirRutes();

            } catch (XmlRpcException e) {
                e.printStackTrace();
            }

            return al;
        }

        private void afegirRutes() throws XmlRpcException {
            TodoListDBManager_Ruta todoListDBManager_ruta = new TodoListDBManager_Ruta(context);
            ArrayList<Ruta> rutes = new ArrayList<>();

            List l = asList((Object[]) ConexionBD.client.execute("execute_kw", asList(ConexionBD.DB, ConexionBD.uid, ConexionBD.PASS, "batoilogic.ruta", "search_read",
                    asList(asList(
                            asList("repartidor_id", "=", rA.getId())
                    )), new HashMap() {
                        {
                            put("fields", asList("repartidor_id", "id"));
                        }
                    }
            )));

            for (int i = 0; i < l.size(); i++) {

                HashMap hm = (HashMap) l.get(i);
                Ruta r = new Ruta();
                r.setId((Integer) hm.get("id"));
                Object[] rutas = (Object[]) hm.get("repartidor_id");
                r.setRepartidor((Integer) rutas[0]);
                afegirElements(r.getId());
                System.out.println("\t\t\t\t--------> ID RUTA" + r.getId());
                todoListDBManager_ruta.insertRuta(r);

            }

        }

        private ArrayList<Albaran> afegirElements(int idR) throws XmlRpcException {
            TodoListDBManager_Albaran todoListDBManager_albaran = new TodoListDBManager_Albaran(context);
            Albaran albaran = null;
            ArrayList<Albaran> albarans = new ArrayList<>();
            List l = asList((Object[]) ConexionBD.client.execute("execute_kw", asList(ConexionBD.DB, ConexionBD.uid, ConexionBD.PASS, "batoilogic.albaran", "search_read",
                    asList(asList(
                            asList("ruta_id", "=", idR)
                    )), new HashMap() {
                        {
                            put("fields", asList("pedido_id", "ruta_id", "id"));
                        }
                    }
            )));


            for (int i = 0; i < l.size(); i++) {

                HashMap hm = (HashMap) l.get(i);
                Object[] pedidos = (Object[]) hm.get("pedido_id");
                Albaran a = new Albaran();
                Object[] rutas = (Object[]) hm.get("ruta_id");
                a.setRuta((Integer) rutas[0]);
                if (a.getRuta() == idR) {


                    try {

                        a.setPedido((Integer) pedidos[0]);
                        pedidoFindByPK((Integer) pedidos[0]);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                    a.setRuta(1);
                    todoListDBManager_albaran.insertAlbaran(a);
                    albarans.add(a);
                }
            }
            return albarans;
        }


        public void pedidoFindByPK(int id) throws Exception {
            TodoListDBManager_Pedido todoListDBManager_pedido = new TodoListDBManager_Pedido(context);
            Pedido pedido = null;
            ArrayList<Pedido> pedidos = new ArrayList<>();
            List l = asList((Object[]) ConexionBD.client.execute("execute_kw", asList(
                    ConexionBD.DB, ConexionBD.uid, ConexionBD.PASS,
                    "batoilogic.pedido", "search_read",
                    asList(asList(
                            asList("id", "=", id),
                            asList("estado", "=", "Preparado")
                    )), new HashMap() {
                        {
                            put("fields", asList("id", "observacion", "fechapedido", "fechaestimada", "estado", "preciototal", "cliente_id", "direccion_id"));
                        }
                    }
            )));
            System.out.println("\n\n" + l.size() + "\t-> id pedidos" + id + "\n\n");
            for (int i = 0; i < l.size(); i++) {

                HashMap hm = (HashMap) l.get(i);
                Pedido p = new Pedido();
                p.setId((Integer) hm.get("id"));
                p.setObservaciones(hm.get("observacion").toString());
                p.setFechaPedido(hm.get("fechapedido").toString());
                p.setFechaEstimada(hm.get("fechaestimada").toString());
                p.setEstado("ER");
                p.setPrecioTotal((Double) hm.get("preciototal"));


                Object[] clientes = (Object[]) hm.get("cliente_id");
//                Cliente c = clienteFindByPk((Integer) clientes[0]);
                p.setCliente((Integer) clientes[0]);

                Object[] direcciones = (Object[]) hm.get("direccion_id");
                Direccion d = direccionFindByPk((Integer) direcciones[0]);
                p.setDireccion((Integer) direcciones[0]);
                CodigoPostal cp = cpFindByPk(d.getCodigo_postal());

                System.out.println("\t\t\t\t\t------------>Codigo postal" + cp.getCodigo());
                Ciudad ciudad = ciudadFindByPk(cp.getId());
                p.setCiudad(ciudad.getId());

                System.out.println("\t\t\t\t\t------------>Pedido ID" + p.getId());
                updatePedidoEstado(p);
                pedidos.add(p);

            }

            boolean semaforo = true;

            for (int i = 0; i < pedidos.size() && semaforo; i++) {

                if (pedidos.get(i).getId() == id) {

                    semaforo = false;
                    pedido = pedidos.get(i);

                }
            }
//            cpFindByPkInsertat();
            clienteFindByPk(pedido.getCliente());
            ciudadFindByPkInsertar(pedido.getCiudad());
            direccionFindByPkInsert(pedido.getDireccion());
            todoListDBManager_pedido.insertPedido(pedido);
        }

        public void updatePedidoEstado(Pedido t) throws Exception {

            ConexionBD.client.execute("execute_kw", asList(
                    ConexionBD.DB, ConexionBD.uid, ConexionBD.PASS,
                    "batoilogic.pedido", "write",
                    asList(asList(t.getId()), new HashMap() {
                                {
                                    put("estado", t.getEstado());
                                }
                            }
                    )
            ));

        }

        public CodigoPostal cpFindByPk(int cp) throws Exception {
            TodoListDBManager_CodigoPostal todoListDBManager_codigoPostal = new TodoListDBManager_CodigoPostal(context);

            CodigoPostal codi = null;
            ArrayList<CodigoPostal> codis = new ArrayList<>();
            List l = asList((Object[]) ConexionBD.client.execute("execute_kw", asList(
                    ConexionBD.DB, ConexionBD.uid, ConexionBD.PASS,
                    "batoilogic.postal", "search_read",
                    asList(asList(

                    )), new HashMap() {
                        {
                            put("fields", asList("id", "numero", "ciudad_id"));
                        }
                    }
            )));

            for (int i = 0; i < l.size(); i++) {

                HashMap hm = (HashMap) l.get(i);
                CodigoPostal c = new CodigoPostal();
                c.setCodigo(hm.get("numero").toString());
                c.setId((Integer) hm.get("id"));
                Object[] ciudades = (Object[]) hm.get("ciudad_id");
                c.setCiudad((Integer) ciudades[0]);

                codis.add(c);
            }
            boolean semaforo = true;

            for (int i = 0; i < codis.size() && semaforo; i++) {

                if (codis.get(i).getId() == cp) {

                    semaforo = false;
                    codi = codis.get(i);


                }
            }
            todoListDBManager_codigoPostal.insertCodigoPostal(codi);

            return codi;

        }


        public void cpFindByPkInsertat() throws Exception {
            TodoListDBManager_CodigoPostal todoListDBManager_codigoPostal = new TodoListDBManager_CodigoPostal(context);
            CodigoPostal codi = null;
            ArrayList<CodigoPostal> codis = new ArrayList<>();
            List l = asList((Object[]) ConexionBD.client.execute("execute_kw", asList(
                    ConexionBD.DB, ConexionBD.uid, ConexionBD.PASS,
                    "batoilogic.postal", "search_read",
                    asList(asList(

                    )), new HashMap() {
                        {
                            put("fields", asList("id", "numero", "ciudad_id"));
                        }
                    }
            )));

            for (int i = 0; i < l.size(); i++) {

                HashMap hm = (HashMap) l.get(i);
                CodigoPostal c = new CodigoPostal();
                c.setCodigo(hm.get("numero").toString());
                c.setId((Integer) hm.get("id"));
                Object[] ciudades = (Object[]) hm.get("ciudad_id");
                c.setCiudad((Integer) ciudades[0]);

//                todoListDBManager_codigoPostal.insertCodigoPostal(c);

                codis.add(c);
            }

        }


        public Ciudad ciudadFindByPk(int idCiudad) throws Exception {
            Ciudad ciudad = null;
            ArrayList<Ciudad> ciudades = new ArrayList<>();
            List l = asList((Object[]) ConexionBD.client.execute("execute_kw", asList(
                    ConexionBD.DB, ConexionBD.uid, ConexionBD.PASS,
                    "batoilogic.ciudad", "search_read",
                    asList(asList(
                            asList("id", "=", idCiudad)
                    )), new HashMap() {
                        {
                            put("fields", asList("id", "nombre"));
                        }
                    }
            )));

            for (int i = 0; i < l.size(); i++) {

                HashMap hm = (HashMap) l.get(i);
                Ciudad c = new Ciudad();
                c.setId((Integer) hm.get("id"));
                c.setNombre(hm.get("nombre").toString());

                ciudades.add(c);
            }

            boolean semaforo = true;

            for (int i = 0; i < ciudades.size() && semaforo; i++) {

                if (ciudades.get(i).getId() == idCiudad) {

                    semaforo = false;
                    ciudad = ciudades.get(i);


                }
            }
            return ciudad;


        }

        public void ciudadFindByPkInsertar(int idCiudad) throws Exception {
            TodoListDBManager_Ciudad todoListDBManager_ciudad = new TodoListDBManager_Ciudad(context);
            Ciudad ciudad = null;
            ArrayList<Ciudad> ciudades = new ArrayList<>();
            List l = asList((Object[]) ConexionBD.client.execute("execute_kw", asList(
                    ConexionBD.DB, ConexionBD.uid, ConexionBD.PASS,
                    "batoilogic.ciudad", "search_read",
                    asList(asList(
                            asList("id", "=", idCiudad)
                    )), new HashMap() {
                        {
                            put("fields", asList("id", "nombre"));
                        }
                    }
            )));

            for (int i = 0; i < l.size(); i++) {

                HashMap hm = (HashMap) l.get(i);
                Ciudad c = new Ciudad();
                c.setId((Integer) hm.get("id"));
                c.setNombre(hm.get("nombre").toString());

                ciudades.add(c);
            }

            boolean semaforo = true;

            for (int i = 0; i < ciudades.size() && semaforo; i++) {

                if (ciudades.get(i).getId() == idCiudad) {

                    semaforo = false;
                    ciudad = ciudades.get(i);


                }
            }
            todoListDBManager_ciudad.insertCiudad(ciudad);


        }


        public Cliente clienteFindByPk(int idCliente) throws Exception {

            TodoListDBManager_Cliente todoListDBManager_cliente = new TodoListDBManager_Cliente(context);
            Cliente cliente = null;
            ArrayList<Cliente> clientes = new ArrayList<>();
            List l = asList((Object[]) ConexionBD.client.execute("execute_kw", asList(
                    ConexionBD.DB, ConexionBD.uid, ConexionBD.PASS,
                    "batoilogic.cliente", "search_read",
                    asList(asList(
                            asList("id", "=", idCliente)
                    )), new HashMap() {
                        {
                            put("fields", asList("id", "nombre", "apellidos", "fechanacimiento"));
                        }
                    }
            )));

            for (int i = 0; i < l.size(); i++) {

                HashMap hm = (HashMap) l.get(i);
                Cliente c = new Cliente();
                c.setId(idCliente);
                c.setNombre(hm.get("nombre").toString());
                c.setApellidos(hm.get("apellidos").toString());
                c.setFechaNacimiento(hm.get("fechanacimiento").toString());

                clientes.add(c);
            }

            boolean semaforo = true;

            for (int i = 0; i < clientes.size() && semaforo; i++) {

                if (clientes.get(i).getId() == idCliente) {

                    semaforo = false;
                    cliente = clientes.get(i);


                }
            }
            todoListDBManager_cliente.insertCliente(cliente);
            return cliente;
        }


        public Direccion direccionFindByPk(int idDireccion) throws Exception {

            Direccion direccion = null;
            ArrayList<Direccion> direcciones = new ArrayList<>();
            List l = asList((Object[]) ConexionBD.client.execute("execute_kw", asList(
                    ConexionBD.DB, ConexionBD.uid, ConexionBD.PASS,
                    "batoilogic.direccion", "search_read",
                    asList(asList(
                            asList("id", "=", idDireccion)
                    )), new HashMap() {
                        {
                            put("fields", asList("numero", "piso", "puerta", "tipo", "nombre", "id", "postal_id"));
                        }
                    }
            )));

            for (int i = 0; i < l.size(); i++) {

                HashMap hm = (HashMap) l.get(i);
                Direccion d = new Direccion();
                d.setNombre(hm.get("nombre").toString());


                d.setNumero((Integer) hm.get("numero"));


                d.setPuerta(hm.get("puerta").toString());
                d.setPiso((Integer) hm.get("piso"));
                d.setTipo((String) hm.get("tipo"));

                d.setId((Integer) hm.get("id"));
                Object[] codigos = (Object[]) hm.get("postal_id");
                d.setCodigo_postal((Integer) codigos[0]);
                direcciones.add(d);
            }

            boolean semaforo = true;

            for (int i = 0; i < direcciones.size() && semaforo; i++) {

                if (direcciones.get(i).getId() == idDireccion) {

                    semaforo = false;
                    direccion = direcciones.get(i);


                }
            }


            return direccion;
        }

        public Direccion direccionFindByPkInsert(int idDireccion) throws Exception {
            TodoListDBManager_Direccion todoListDBManager_direccion = new TodoListDBManager_Direccion(context);
            Direccion direccion = null;
            ArrayList<Direccion> direcciones = new ArrayList<>();
            List l = asList((Object[]) ConexionBD.client.execute("execute_kw", asList(
                    ConexionBD.DB, ConexionBD.uid, ConexionBD.PASS,
                    "batoilogic.direccion", "search_read",
                    asList(asList(
                            asList("id", "=", idDireccion)
                    )), new HashMap() {
                        {
                            put("fields", asList("numero", "piso", "puerta", "tipo", "nombre", "id", "postal_id"));
                        }
                    }
            )));

            for (int i = 0; i < l.size(); i++) {

                HashMap hm = (HashMap) l.get(i);
                Direccion d = new Direccion();
                d.setNombre(hm.get("nombre").toString());


                d.setNumero((Integer) hm.get("numero"));


                d.setPuerta(hm.get("puerta").toString());
                d.setPiso((Integer) hm.get("piso"));
                d.setTipo((String) hm.get("tipo"));

                d.setId((Integer) hm.get("id"));
                Object[] codigos = (Object[]) hm.get("postal_id");
                d.setCodigo_postal((Integer) codigos[0]);
                direcciones.add(d);
            }

            boolean semaforo = true;

            for (int i = 0; i < direcciones.size() && semaforo; i++) {

                if (direcciones.get(i).getId() == idDireccion) {

                    semaforo = false;
                    direccion = direcciones.get(i);


                }
            }
            todoListDBManager_direccion.insertDireccion(direccion);

            return direccion;
        }

        @Override
        protected void onPostExecute(ArrayList<Albaran> searchResult) {
//            progressBar.setVisibility(View.GONE);
//            mAdapter.notifyDataSetChanged();


        }

    }
}

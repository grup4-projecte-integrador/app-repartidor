package grup4.projecte.integrador.batoilogicdeliveries.Activities;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import grup4.projecte.integrador.batoilogicdeliveries.Modelos.Albaran;
import grup4.projecte.integrador.batoilogicdeliveries.R;

public class ModifyAlbaranActivity extends AppCompatActivity {

    private TextView tvNombreCliente;
    private CheckBox cbEntregado;
    private EditText etMotivo;
    private TextView tvId;
    private Albaran item;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_modify_albaran);

        setUI();
    }

    public void setUI() {


        tvNombreCliente = findViewById(R.id.tvNombreCliente);
        cbEntregado = findViewById(R.id.cbEntregado);
        etMotivo = findViewById(R.id.etMotivo);
        tvId = findViewById(R.id.tvIdPedido);

        Intent intent = getIntent();

        item = (Albaran) intent.getSerializableExtra("Item");

        tvNombreCliente.setText(item.getPedido().getCliente().getNombre() + " " + item.getPedido().getCliente().getApellidos());

        if (item.getPedido().getEstado().equals("E")) {

            cbEntregado.setChecked(true);

        } else {

            cbEntregado.setChecked(false);

        }


        if (!item.getPedido().getObservaciones().isEmpty()) {

            etMotivo.setText(item.getPedido().getObservaciones());

        }

        tvId.setText(item.getPedido().getId() + "");


    }

    public void onClick(View view) {

        if (cbEntregado.isChecked()) {
            item.getPedido().setEstado("E");
        } else {
            item.getPedido().setEstado("NE");
        }
        item.getPedido().setObservaciones(etMotivo.getText().toString());
        Intent i = new Intent();
        i.putExtra("item", item);
        setResult(1, i);
        finish();
    }
}

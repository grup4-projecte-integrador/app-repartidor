package grup4.projecte.integrador.batoilogicdeliveries.Activities;

import androidx.appcompat.app.AppCompatActivity;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.view.View;

import grup4.projecte.integrador.batoilogicdeliveries.R;
import grup4.projecte.integrador.batoilogicdeliveries.database_Local.Clonar_Local;

public class MainActivity extends AppCompatActivity {
    Clonar_Local clonar_local;
    private static final int LOCATION_REQUEST = 1337 + 3;

    private static final String[] INITIAL_PERMS = {
            Manifest.permission.ACCESS_FINE_LOCATION,
            Manifest.permission.READ_CONTACTS
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        if (!canAccessLocation()) {
            requestPermissions(INITIAL_PERMS, LOCATION_REQUEST);
        }
//        clonar_local = new Clonar_Local(this.getApplicationContext());

    }


    public void llistarEntregues(View view) {
        startActivity(new Intent(getApplicationContext(), AlbaranActivity.class));
        finish();
    }

    private boolean canAccessLocation() {
        return (hasPermission(Manifest.permission.ACCESS_FINE_LOCATION));
    }

    private boolean hasPermission(String perm) {
        return (PackageManager.PERMISSION_GRANTED == checkSelfPermission(perm));
    }
}

package grup4.projecte.integrador.batoilogicdeliveries.Activities;

import androidx.core.app.ActivityCompat;
import androidx.fragment.app.FragmentActivity;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationManager;
import android.os.Bundle;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptor;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import java.io.IOException;
import java.util.List;
import java.util.Locale;

import grup4.projecte.integrador.batoilogicdeliveries.R;

public class SinglePointMapActivity extends FragmentActivity implements OnMapReadyCallback {

    private GoogleMap mMap;
    private String direccio;
    private String nombreMapa;
    private LocationManager locationManager;
    private Marker marker;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ubicacio_casa);
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);

        Intent i = getIntent();

        direccio = i.getStringExtra("Ciudad") + "," + i.getStringExtra("NombreCalle") + ", " + i.getIntExtra("Numero", 10);
        nombreMapa = i.getStringExtra("NombreCliente") + " " + i.getStringExtra("ApellidosCliente") + ", " + i.getStringExtra("NombreCalle") + " Nº" + i.getIntExtra("Numero", 10);
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        try {
            mMap = googleMap;

            Location location = null;
            LatLng latLng = null;

            if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) {

                locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
                location = locationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);
            }
            System.out.println("\t\t--------->Abans");
            System.out.println("\t\t"+location.getLongitude());
            latLng = new LatLng(location.getLatitude(), location.getLongitude());

            mMap.setMyLocationEnabled(true);
            mMap.getUiSettings().setMyLocationButtonEnabled(true);
            Geocoder geocoder = new Geocoder(this, Locale.getDefault());
            List<Address> addresses;
            addresses = geocoder.getFromLocationName(direccio, 1);

            if (addresses.size() > 0) {

                double latitude = addresses.get(0).getLatitude();
                double longitude = addresses.get(0).getLongitude();

                LatLng punto = new LatLng(latitude, longitude);


                marker = mMap.addMarker(new MarkerOptions().icon(BitmapDescriptorFactory.fromResource(R.drawable.iconopaquete)).position(punto).title(nombreMapa));

                float zoom = 16;
                mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(punto, zoom));

            }


        } catch (IOException e) {

            e.printStackTrace();
        }
    }


}

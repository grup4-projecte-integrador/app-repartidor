package grup4.projecte.integrador.batoilogicdeliveries.Modelos_Internos;

import java.io.Serializable;

public class Ruta implements Serializable {

    public int id;
    public int repartidor;

    public Ruta() {
    }

    public Ruta(int id, int repartidor) {
        this.id = id;
        this.repartidor = repartidor;
    }

    public Ruta(Ruta ruta) {
        this.id = ruta.getId();
        this.repartidor = ruta.getRepartidor();
    }


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getRepartidor() {
        return repartidor;
    }

    public void setRepartidor(int repartidor) {
        this.repartidor = repartidor;
    }
}

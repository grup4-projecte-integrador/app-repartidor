package grup4.projecte.integrador.batoilogicdeliveries.database_Local;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import java.util.ArrayList;

import grup4.projecte.integrador.batoilogicdeliveries.Modelos_Internos.Albaran;


public class TodoListDBManager_Albaran {
    private TodoListDBHelper todoListDBHelper;

    public TodoListDBManager_Albaran(Context context) {
        todoListDBHelper = TodoListDBHelper.getInstance(context);
    }

    // Operations

    // CREATE new ros
    public void insertAlbaran(Albaran albaran) {
        // open database to read and write
        SQLiteDatabase sqLiteDatabase = todoListDBHelper.getWritableDatabase();
        int id = albaran.getId();
        int id_pedido= albaran.getPedido();
        int ruta_id = albaran.getRuta();
        if (sqLiteDatabase != null) {
            ContentValues contentValue = new ContentValues();

            contentValue.put(TodoListDBContract.Albarans.PEDIDO_ID, id_pedido);
            contentValue.put(TodoListDBContract.Albarans.RUTA_ID, ruta_id);


            sqLiteDatabase.insert(TodoListDBContract.Albarans.TABLE_NAME, null, contentValue);

            // getWritableDatabase & getReadableDatabase() are expensive, you should leave your database connection
            // open for as long as you possibly need to access it
            //
            // sqLiteDatabase.close();
        }
    }


    // Get all data from Albarans table
    public ArrayList<Albaran> getAlbarans() {
        ArrayList<Albaran> albaranList = new ArrayList<>();

        // open database to read
        SQLiteDatabase sqLiteDatabase = todoListDBHelper.getReadableDatabase();

        if (sqLiteDatabase != null) {
            String[] projection = new String[]{TodoListDBContract.Albarans._ID,
                    TodoListDBContract.Albarans.PEDIDO_ID,
                    TodoListDBContract.Albarans.RUTA_ID};


            Cursor cursorTodoList = sqLiteDatabase.query(TodoListDBContract.Albarans.TABLE_NAME,
                    projection,                     // The columns toView return
                    null,                  // no WHERE clause
                    null,               // no values for the WHERE clause
                    null,                   // don't group hte rows
                    null,                    // don't filter by row groups
                    null);                  // don't sort rows


            if (cursorTodoList != null) {
                // get the column indexes for required columns
                int _idIndex = cursorTodoList.getColumnIndexOrThrow(TodoListDBContract.Albarans._ID);
                int pedidoIndex = cursorTodoList.getColumnIndexOrThrow(TodoListDBContract.Albarans.PEDIDO_ID);
                int rutaIndex = cursorTodoList.getColumnIndexOrThrow(TodoListDBContract.Albarans.RUTA_ID);

                //read data and add to ArrayList
                while (cursorTodoList.moveToNext()) {
                    Albaran albaran = new Albaran(cursorTodoList.getInt(_idIndex),
                            cursorTodoList.getInt(pedidoIndex),
                            cursorTodoList.getInt(rutaIndex));

                    albaranList.add(albaran);
                }
                // close cursor to free resources
                cursorTodoList.close();
            }
        }
        return albaranList;
    }

    public void close() {
        todoListDBHelper.close();       // closes any opened database object
    }

    /**
     * Aci actualitzem el contingut de cada element al obrir el EditTaskActivity
     *
     * @param id
     */
    public void update(int id, int id_pedido, int ruta_id) {
        SQLiteDatabase sqLiteDatabase = todoListDBHelper.getWritableDatabase();

        if (sqLiteDatabase != null) {
            ContentValues contentValue = new ContentValues();

            contentValue.put(TodoListDBContract.Albarans.PEDIDO_ID, id_pedido);
            contentValue.put(TodoListDBContract.Albarans.RUTA_ID, ruta_id);


            sqLiteDatabase.update(TodoListDBContract.Albarans.TABLE_NAME, contentValue, "_id=" + id, null);


        }
    }

    public void deleteAlbarans(int id) {
        SQLiteDatabase sqLiteDatabase = todoListDBHelper.getWritableDatabase();
        if (sqLiteDatabase != null) {
            sqLiteDatabase.delete(TodoListDBContract.Albarans.TABLE_NAME, "_id=" + id, null);

        }

    }

    public void deleteComplAlbarans() {
        SQLiteDatabase sqLiteDatabase = todoListDBHelper.getWritableDatabase();
        String stat = "Completed";
        if (sqLiteDatabase != null) {
//            sqLiteDatabase.delete(TodoListDBContract.Albarans.TABLE_NAME, TodoListDBContract.Albarans.STATUS + "=" + stat, null);
            sqLiteDatabase.execSQL("DELETE FROM " + TodoListDBContract.Albarans.TABLE_NAME + " WHERE status='Completed'");
        }
    }

    public void deleteAllAlbarans() {
        SQLiteDatabase sqLiteDatabase = todoListDBHelper.getWritableDatabase();
        if (sqLiteDatabase != null) {
            sqLiteDatabase.delete(TodoListDBContract.Albarans.TABLE_NAME, null, null);

        }

    }


    public ArrayList<Albaran> getSelection(String stat) {
        ArrayList<Albaran> albaranList = new ArrayList<>();
        String[] tipus = new String[]{stat};
        System.out.println("\t\t\t---------------->" + stat);

        // open database to read
        SQLiteDatabase sqLiteDatabase = todoListDBHelper.getReadableDatabase();

        if (sqLiteDatabase != null) {
            String[] projection = new String[]{TodoListDBContract.Albarans._ID,
                    TodoListDBContract.Albarans.PEDIDO_ID,
                    TodoListDBContract.Albarans.RUTA_ID
            };
//
//            taskList.clear();
            System.out.println(albaranList.size());
//
            Cursor cursorTodoList = sqLiteDatabase.query(TodoListDBContract.Albarans.TABLE_NAME,
                    projection,                     // The columns toView return
                    "status=?",                  // no WHERE clause
                    tipus,               // no values for the WHERE clause
                    null,                   // don't group hte rows
                    null,                    // don't filter by row groups
                    null);                  // don't sort rows


            if (cursorTodoList != null) {
                // get the column indexes for required columns
                int _idIndex = cursorTodoList.getColumnIndexOrThrow(TodoListDBContract.Albarans._ID);
                int pedidoIndex = cursorTodoList.getColumnIndexOrThrow(TodoListDBContract.Albarans.PEDIDO_ID);
                int rutaIndex = cursorTodoList.getColumnIndexOrThrow(TodoListDBContract.Albarans.RUTA_ID);

                //read data and add to ArrayList
                while (cursorTodoList.moveToNext()) {
                    Albaran albaran = new Albaran(cursorTodoList.getInt(_idIndex),
                            cursorTodoList.getInt(pedidoIndex),
                            cursorTodoList.getInt(rutaIndex));
                    albaranList.add(albaran);
                    System.out.println("\t\t" + cursorTodoList.getInt(_idIndex));
                }
                System.out.println("\t\t\t\t\t\t--------->" + albaranList.size());
                // close cursor to free resources
                cursorTodoList.close();
            }
        }
        return albaranList;
    }


}

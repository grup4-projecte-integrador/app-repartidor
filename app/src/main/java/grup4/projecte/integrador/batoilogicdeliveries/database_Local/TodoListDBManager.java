package grup4.projecte.integrador.batoilogicdeliveries.database_Local;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import java.sql.PreparedStatement;
import java.util.ArrayList;

import grup4.projecte.integrador.batoilogicdeliveries.Modelos.Albaran;
import grup4.projecte.integrador.batoilogicdeliveries.Modelos.Cliente;
import grup4.projecte.integrador.batoilogicdeliveries.Modelos.Direccion;
import grup4.projecte.integrador.batoilogicdeliveries.Modelos.Pedido;
import grup4.projecte.integrador.batoilogicdeliveries.Modelos.Repartidor;
import grup4.projecte.integrador.batoilogicdeliveries.Modelos.Repartidor_Actual;


public class TodoListDBManager {

    Repartidor r;
    private TodoListDBHelper todoListDBHelper;
    String myquery;

    public TodoListDBManager(Context context) {
        todoListDBHelper = TodoListDBHelper.getInstance(context);
        Repartidor_Actual rA = new Repartidor_Actual();
        r = rA.getRepartidor();
        myquery = "select p._id, p.observacion, p.estado, d.nombre, d.tipo, d.numero, d.piso, d.puerta, cl.nombre, cl.apellidos, ci.nombre " +
                "from Albarans a " +
                "inner join Rutas br on br._id = a.ruta_id " +
                "inner join Pedidos p on a.pedido_id = p._id " +
                "inner join Direcciones d on d._id = p.direccion_id " +
                "inner join Clientes cl on cl._id = d.cliente_id " +
                "inner join Codigo_Postal po on po._id = d.postal_id " +
                "inner join Ciudades ci on ci._id = po.ciudad " +
                "where br.repartidor_id = " + r.getId();
    }

    public ArrayList<Albaran> agafar() {
        System.out.println("\t\t\t\t---------> agafar");
        System.out.println("\t\t" + myquery);

        SQLiteDatabase db =todoListDBHelper.getWritableDatabase();

//        Cursor c = todoListDBHelper.getWritableDatabase().rawQuery(myquery, null);

        Cursor c = db.rawQuery(myquery, null);


        System.out.println("\t\t\t\t--------> despres");
        ArrayList<Albaran> albarans = new ArrayList<>();
        int cant = c.getCount();
        System.out.println("\t\t\t\t--------> Cant " + cant);
        while (c.moveToNext()) {
            System.out.println("\t\t\t\t---------> dins del while");
            Direccion d = new Direccion();
            d.setNombre(c.getString(4));
            d.setTipo(c.getString(5));
            d.setNumero(c.getInt(6));
            d.setPiso(c.getInt(7));
            d.setPuerta(c.getString(8));
            Cliente cl = new Cliente();
            cl.setNombre(c.getString(9));
            cl.setApellidos(c.getString(10));
            Pedido p = new Pedido();
            p.setId(c.getInt(1));
            p.setObservaciones(c.getString(2));
            p.setEstado(c.getString(3));
            p.setDireccion(d);
            p.setCliente(cl);
            Albaran a = new Albaran();
            a.setPedido(p);
            System.out.println("\t\t\t\t\t\t----------->cliente " + cl.getNombre());
            albarans.add(a);

        }

        return albarans;
    }




    public ArrayList<Albaran> getAlbarans() {
        ArrayList<Albaran> albaranList = new ArrayList<>();

        SQLiteDatabase sqLiteDatabase = todoListDBHelper.getReadableDatabase();

        if (sqLiteDatabase != null) {
            String[] projection = new String[]{
                    TodoListDBContract.Pedidos._ID,
                    TodoListDBContract.Pedidos.OBSERVACION,
                    TodoListDBContract.Pedidos.ESTADO,
                    TodoListDBContract.Direcciones.NOMBRE,
                    TodoListDBContract.Direcciones.TIPO,
                    TodoListDBContract.Direcciones.NUMERO,
                    TodoListDBContract.Direcciones.PUERTA,
                    TodoListDBContract.Clientes.NOMBRE,
                    TodoListDBContract.Clientes.APELLIDOS,
                    TodoListDBContract.Ciudades.NOMBRE

            };
        }

//        Cursor cursorTodoList = sqLiteDatabase.query(myquery);


        return albaranList;
    }
}

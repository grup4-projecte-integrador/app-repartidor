package grup4.projecte.integrador.batoilogicdeliveries.Activities.ui.home;

import android.app.job.JobInfo;
import android.app.job.JobScheduler;
import android.content.ComponentName;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.ItemTouchHelper;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.floatingactionbutton.FloatingActionButton;

import org.apache.xmlrpc.XmlRpcException;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.concurrent.TimeUnit;

import grup4.projecte.integrador.batoilogicdeliveries.Activities.EveryPointMapActivity;
import grup4.projecte.integrador.batoilogicdeliveries.Activities.GetUbicacion;
import grup4.projecte.integrador.batoilogicdeliveries.Activities.ModifyAlbaranActivity;
import grup4.projecte.integrador.batoilogicdeliveries.Activities.MyAdapter;
import grup4.projecte.integrador.batoilogicdeliveries.Activities.SinglePointMapActivity;
import grup4.projecte.integrador.batoilogicdeliveries.Conexion.ConexionBD;
import grup4.projecte.integrador.batoilogicdeliveries.Modelos.Albaran;
import grup4.projecte.integrador.batoilogicdeliveries.Modelos.Camion;
import grup4.projecte.integrador.batoilogicdeliveries.Modelos.Ciudad;
import grup4.projecte.integrador.batoilogicdeliveries.Modelos.Cliente;
import grup4.projecte.integrador.batoilogicdeliveries.Modelos.CodigoPostal;
import grup4.projecte.integrador.batoilogicdeliveries.Modelos.Direccion;
import grup4.projecte.integrador.batoilogicdeliveries.Modelos.Pedido;
import grup4.projecte.integrador.batoilogicdeliveries.Modelos.Repartidor;
import grup4.projecte.integrador.batoilogicdeliveries.Modelos.Repartidor_Actual;
import grup4.projecte.integrador.batoilogicdeliveries.Modelos.Ruta;
import grup4.projecte.integrador.batoilogicdeliveries.R;
import grup4.projecte.integrador.batoilogicdeliveries.database_Local.TodoListDBManager;

import static android.app.Activity.RESULT_OK;
import static android.content.Context.JOB_SCHEDULER_SERVICE;
import static androidx.core.content.ContextCompat.getSystemService;
import static java.util.Arrays.asList;


public class HomeFragment extends Fragment implements MyAdapter.OnItemClickListener, MyAdapter.OnLongItemClickListener {

    private ArrayList<Albaran> myDataset;
    private RecyclerView.Adapter mAdapter;
    private RecyclerView.LayoutManager layoutManager;
    private ConsultaBD consultaBD;
    private ProgressBar progressBar;
    private Repartidor rAct;
    private Repartidor_Actual rA;
    private Albaran albaranSeleccionado;

    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        albaranSeleccionado = null;
        rA = new Repartidor_Actual();
        rAct = rA.getRepartidor();

        myDataset = new ArrayList<>();
        View root = inflater.inflate(R.layout.activity_albaran, container, false);
        FloatingActionButton fab = root.findViewById(R.id.fabMap);
        progressBar = root.findViewById(R.id.progressBar);

        consultaBD = new ConsultaBD(layoutManager);
        consultaBD.execute();
        RecyclerView rv = root.findViewById(R.id.recyclerViewActivities);
        mAdapter = new MyAdapter(myDataset, this, this);

        rv.setHasFixedSize(true);
        rv.setAdapter(mAdapter);
        layoutManager = new LinearLayoutManager(getContext(), RecyclerView.VERTICAL, false);
        rv.setLayoutManager(layoutManager);

        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(getContext(), EveryPointMapActivity.class);
                Bundle bundle = new Bundle();

                for (int i = 0; i < myDataset.size(); i++) {
                    bundle.putSerializable("ss" + i, (Serializable) myDataset.get(i));
                }

                bundle.putInt("size", myDataset.size());
                intent.putExtra("bundle", bundle);
                startActivity(intent);
            }
        });
        return root;

    }


    @Override
    public void onItemClick(Albaran item) {
        Intent intent = new Intent(this.getContext(), SinglePointMapActivity.class);

        intent.putExtra("NombreCalle", item.getPedido().getDireccion().getNombre());
        intent.putExtra("Ciudad", item.getPedido().getCiudad().getNombre());
        intent.putExtra("Numero", item.getPedido().getDireccion().getNumero());
        intent.putExtra("NombreCliente", item.getPedido().getCliente().getNombre());
        intent.putExtra("ApellidosCliente", item.getPedido().getCliente().getApellidos());

        startActivity(intent);
    }


    @Override
    public boolean onLongItemClick(Albaran item) {

        albaranSeleccionado = item;


        Intent intent = new Intent(this.getContext(), ModifyAlbaranActivity.class);

        intent.putExtra("Item", item);


        intent.putExtra("NombreCliente", item.getPedido().getCliente().getNombre());
        intent.putExtra("IdPedido", item.getPedido().getId());

        if (item.getPedido().getEstado().equals("E")) {

            intent.putExtra("EstadoPedido", true);

        } else {

            intent.putExtra("EstadoPedido", false);

        }

        if (!item.getPedido().getObservaciones().isEmpty()) {

            intent.putExtra("Observaciones", item.getPedido().getObservaciones());

        }

        startActivityForResult(intent, 1);
        return true;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        // Check which request we're responding to
        if (resultCode == 1) {
            albaranSeleccionado = (Albaran) data.getSerializableExtra("item");
            Albaran a = new Albaran();
            for (int i = 0; i < myDataset.size(); i++) {

                if (myDataset.get(i).getId() == albaranSeleccionado.getId()) {
                    myDataset.remove(i);
                }

            }

            Pedido p = new Pedido();
            p = albaranSeleccionado.getPedido();


            updatePedidoEstado(p);

            rAct.getCamion().setCantidad(rAct.getCamion().getCantidad() - 1);
            updateCamionCandidad(rAct.getCamion());
            mAdapter.notifyDataSetChanged();
            if (myDataset.isEmpty()) {

            }
        }
    }

    public void updatePedidoEstado(Pedido t) {
        try {
            ConexionBD.client.execute("execute_kw", asList(
                    ConexionBD.DB, ConexionBD.uid, ConexionBD.PASS,
                    "batoilogic.pedido", "write",
                    asList(asList(t.getId()), new HashMap() {
                                {
                                    put("estado", t.getEstado());
                                }
                            }
                    )
            ));
        } catch (XmlRpcException e) {
            e.printStackTrace();
        }
    }

    public void updateCamionCandidad(Camion c) {
        try {
            ConexionBD.client.execute("execute_kw", asList(
                    ConexionBD.DB, ConexionBD.uid, ConexionBD.PASS,
                    "batoilogic.camion", "write",
                    asList(asList(c.getId()), new HashMap() {
                                {
                                    put("capacidad", c.getCantidad());
                                }
                            }
                    )
            ));
        } catch (XmlRpcException e) {
            e.printStackTrace();
        }
    }

    class ConsultaBD extends AsyncTask<Void, Void, ArrayList<Albaran>> {


        public ArrayList<Albaran> al = new ArrayList<>();
        private final RecyclerView.LayoutManager layoutManager;

        public ConsultaBD(RecyclerView.LayoutManager layoutManager) {

            this.layoutManager = layoutManager;
        }


        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressBar.setVisibility(View.VISIBLE);
        }

        @Override
        protected ArrayList<Albaran> doInBackground(Void... voids) {


            try {
                al = afegirElements();
                if (al.size() > 0) {
                    myDataset.addAll(al);
                } else {
//                    Toast.makeText(getActivity(), "Este repartidor no tiene entregas", Toast.LENGTH_LONG).show();
                }
            } catch (XmlRpcException e) {
                e.printStackTrace();
            }


            return al;
        }


        private ArrayList<Albaran> afegirElements() throws XmlRpcException {


            List lRuta = asList((Object[]) ConexionBD.client.execute("execute_kw", asList(
                    ConexionBD.DB, ConexionBD.uid, ConexionBD.PASS,
                    "batoilogic.ruta", "search_read",
                    asList(asList(
                            asList("repartidor_id", "=", rAct.getId())
                    )), new HashMap() {
                        {
                            put("fields", asList("id"));
                        }
                    }
            )));

            ArrayList<Integer> idRutasList = new ArrayList<>();
            for (int i = 0; i < lRuta.size(); i++) {

                HashMap hm = (HashMap) lRuta.get(i);
                idRutasList.add((int) hm.get("id"));

            }

            ArrayList<Albaran> albarans = new ArrayList<>();
            List l = asList((Object[]) ConexionBD.client.execute("execute_kw", asList(ConexionBD.DB, ConexionBD.uid, ConexionBD.PASS, "batoilogic.albaran", "search_read",
                    asList(asList(
                    )), new HashMap() {
                        {
                            put("fields", asList("pedido_id", "ruta_id", "id"));
                        }
                    }
            )));


            for (int i = 0; i < l.size(); i++) {

                HashMap hm = (HashMap) l.get(i);
                Object[] pedidos = (Object[]) hm.get("pedido_id");


                Albaran a = new Albaran();

                if (idRutasList.contains((int) ((Object[]) hm.get("ruta_id"))[0])) {
                    try {
                        Pedido aux = pedidoFindByPK((Integer) pedidos[0]);
                        Pedido p = pedidoFindByPKER((Integer) pedidos[0]);
                        if (p != null) {
                            a.setPedido(p);
                            a.setId((Integer) hm.get("id"));
                            albarans.add(a);
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }


            return albarans;
        }

        public void updatePedidoEstado(Pedido t) throws Exception {

            ConexionBD.client.execute("execute_kw", asList(
                    ConexionBD.DB, ConexionBD.uid, ConexionBD.PASS,
                    "batoilogic.pedido", "write",
                    asList(asList(t.getId()), new HashMap() {
                                {
                                    put("estado", t.getEstado());
                                }
                            }
                    )
            ));

        }


        public Pedido pedidoFindByPK(int id) throws Exception {

            Pedido pedido = null;
            ArrayList<Pedido> pedidos = new ArrayList<>();
            List l = asList((Object[]) ConexionBD.client.execute("execute_kw", asList(
                    ConexionBD.DB, ConexionBD.uid, ConexionBD.PASS,
                    "batoilogic.pedido", "search_read",
                    asList(asList(
                            asList("id", "=", id),
                            asList("estado", "=", "Preparado")
                    )), new HashMap() {
                        {
                            put("fields", asList("id", "observacion", "fechapedido", "fechaestimada", "estado", "preciototal", "cliente_id", "direccion_id"));
                        }
                    }
            )));

            for (int i = 0; i < l.size(); i++) {

                HashMap hm = (HashMap) l.get(i);
                Pedido p = new Pedido();
                p.setId((Integer) hm.get("id"));
                p.setObservaciones(hm.get("observacion").toString());
                p.setFechaPedido(hm.get("fechapedido").toString());
                p.setFechaEstimada(hm.get("fechaestimada").toString());
                p.setEstado(hm.get("estado").toString());
                p.setPrecioTotal((Double) hm.get("preciototal"));


                Object[] clientes = (Object[]) hm.get("cliente_id");
                Cliente c = clienteFindByPk((Integer) clientes[0]);
                p.setCliente(c);

                Object[] direcciones = (Object[]) hm.get("direccion_id");
                Direccion d = direccionFindByPk((Integer) direcciones[0]);
                p.setDireccion(d);
                CodigoPostal cp = cpFindByPk(d.getCodigo_postal());

                Ciudad ciudad = ciudadFindByPk(cp.getId());
                p.setCiudad(ciudad);
                pedidos.add(p);

            }

            for (Pedido elemento : pedidos) {

                elemento.setEstado("ER");
                updatePedidoEstado(elemento);

            }


            if (pedidos.size() > 0) {
                return pedidos.get(0);
            } else {

                return null;
            }

        }

        public Pedido pedidoFindByPKER(int id) throws Exception {

            Pedido pedido = null;
            ArrayList<Pedido> pedidos = new ArrayList<>();
            List l = asList((Object[]) ConexionBD.client.execute("execute_kw", asList(
                    ConexionBD.DB, ConexionBD.uid, ConexionBD.PASS,
                    "batoilogic.pedido", "search_read",
                    asList(asList(
                            asList("id", "=", id),
                            asList("estado", "=", "ER")
                    )), new HashMap() {
                        {
                            put("fields", asList("id", "observacion", "fechapedido", "fechaestimada", "estado", "preciototal", "cliente_id", "direccion_id"));
                        }
                    }
            )));

            for (int i = 0; i < l.size(); i++) {

                HashMap hm = (HashMap) l.get(i);
                Pedido p = new Pedido();
                p.setId((Integer) hm.get("id"));
                p.setObservaciones(hm.get("observacion").toString());
                p.setFechaPedido(hm.get("fechapedido").toString());
                p.setFechaEstimada(hm.get("fechaestimada").toString());
                p.setEstado(hm.get("estado").toString());
                p.setPrecioTotal((Double) hm.get("preciototal"));


                Object[] clientes = (Object[]) hm.get("cliente_id");
                Cliente c = clienteFindByPk((Integer) clientes[0]);
                p.setCliente(c);

                Object[] direcciones = (Object[]) hm.get("direccion_id");
                Direccion d = direccionFindByPk((Integer) direcciones[0]);
                p.setDireccion(d);
                CodigoPostal cp = cpFindByPk(d.getCodigo_postal());

                Ciudad ciudad = ciudadFindByPk(cp.getId());
                p.setCiudad(ciudad);
                pedidos.add(p);

            }

            if (pedidos.size() > 0) {
                return pedidos.get(0);
            } else {

                return null;
            }

        }

        public CodigoPostal cpFindByPk(int cp) throws Exception {

            CodigoPostal codi = null;
            ArrayList<CodigoPostal> codis = new ArrayList<>();
            List l = asList((Object[]) ConexionBD.client.execute("execute_kw", asList(
                    ConexionBD.DB, ConexionBD.uid, ConexionBD.PASS,
                    "batoilogic.postal", "search_read",
                    asList(asList(
                            asList("id", "=", cp)
                    )), new HashMap() {
                        {
                            put("fields", asList("id", "numero", "ciudad_id"));
                        }
                    }
            )));

            for (int i = 0; i < l.size(); i++) {

                HashMap hm = (HashMap) l.get(i);
                CodigoPostal c = new CodigoPostal();
                c.setCodigo(hm.get("numero").toString());
                c.setId((Integer) hm.get("id"));
                Object[] ciudades = (Object[]) hm.get("ciudad_id");
                c.setCiudad(ciudadFindByPk((Integer) ciudades[0]));
                codis.add(c);
            }

            boolean semaforo = true;

            for (int i = 0; i < codis.size() && semaforo; i++) {

                if (codis.get(i).getId() == cp) {

                    semaforo = false;
                    codi = codis.get(i);


                }
            }

            return codi;
        }


        public Ciudad ciudadFindByPk(int idCiudad) throws Exception {
            Ciudad ciudad = null;
            ArrayList<Ciudad> ciudades = new ArrayList<>();
            List l = asList((Object[]) ConexionBD.client.execute("execute_kw", asList(
                    ConexionBD.DB, ConexionBD.uid, ConexionBD.PASS,
                    "batoilogic.ciudad", "search_read",
                    asList(asList(
                            asList("id", "=", idCiudad)
                    )), new HashMap() {
                        {
                            put("fields", asList("id", "nombre"));
                        }
                    }
            )));

            for (int i = 0; i < l.size(); i++) {

                HashMap hm = (HashMap) l.get(i);
                Ciudad c = new Ciudad();
                c.setId((Integer) hm.get("id"));
                c.setNombre(hm.get("nombre").toString());
                ciudades.add(c);
            }

            boolean semaforo = true;

            for (int i = 0; i < ciudades.size() && semaforo; i++) {

                if (ciudades.get(i).getId() == idCiudad) {

                    semaforo = false;
                    ciudad = ciudades.get(i);


                }
            }

            return ciudad;


        }


        public Cliente clienteFindByPk(int idCliente) throws Exception {


            Cliente cliente = null;
            ArrayList<Cliente> clientes = new ArrayList<>();
            List l = asList((Object[]) ConexionBD.client.execute("execute_kw", asList(
                    ConexionBD.DB, ConexionBD.uid, ConexionBD.PASS,
                    "batoilogic.cliente", "search_read",
                    asList(asList(
                            asList("id", "=", idCliente)
                    )), new HashMap() {
                        {
                            put("fields", asList("id", "nombre", "apellidos", "fechanacimiento"));
                        }
                    }
            )));

            for (int i = 0; i < l.size(); i++) {

                HashMap hm = (HashMap) l.get(i);
                Cliente c = new Cliente();
                c.setId(idCliente);
                c.setNombre(hm.get("nombre").toString());
                c.setApellidos(hm.get("apellidos").toString());
                c.setFechaNacimiento(hm.get("fechanacimiento").toString());
                clientes.add(c);
            }

            boolean semaforo = true;

            for (int i = 0; i < clientes.size() && semaforo; i++) {

                if (clientes.get(i).getId() == idCliente) {

                    semaforo = false;
                    cliente = clientes.get(i);


                }
            }

            return cliente;
        }


        public Direccion direccionFindByPk(int idDireccion) throws Exception {

            Direccion direccion = null;
            ArrayList<Direccion> direcciones = new ArrayList<>();
            List l = asList((Object[]) ConexionBD.client.execute("execute_kw", asList(
                    ConexionBD.DB, ConexionBD.uid, ConexionBD.PASS,
                    "batoilogic.direccion", "search_read",
                    asList(asList(
                            asList("id", "=", idDireccion)
                    )), new HashMap() {
                        {
                            put("fields", asList("numero", "piso", "puerta", "tipo", "nombre", "id", "postal_id"));
                        }
                    }
            )));

            for (int i = 0; i < l.size(); i++) {

                HashMap hm = (HashMap) l.get(i);
                Direccion d = new Direccion();
                d.setNombre(hm.get("nombre").toString());


                d.setNumero((Integer) hm.get("numero"));


                d.setPuerta(hm.get("puerta").toString());
                d.setPiso((Integer) hm.get("piso"));
                d.setTipo((String) hm.get("tipo"));

                d.setId((Integer) hm.get("id"));
                Object[] codigos = (Object[]) hm.get("postal_id");
                d.setCodigo_postal((Integer) codigos[0]);
                direcciones.add(d);
            }

            boolean semaforo = true;

            for (int i = 0; i < direcciones.size() && semaforo; i++) {

                if (direcciones.get(i).getId() == idDireccion) {

                    semaforo = false;
                    direccion = direcciones.get(i);


                }
            }

            return direccion;
        }


        @Override
        protected void onPostExecute(ArrayList<Albaran> searchResult) {

            progressBar.setVisibility(View.GONE);
            mAdapter.notifyDataSetChanged();


        }

    }


}
package grup4.projecte.integrador.batoilogicdeliveries.Modelos_Internos;

import java.io.Serializable;

public class Cliente implements Serializable {

    public String apellidos;
    public String nombre;
    public String fechaNacimiento;
    public int id;

    public Cliente(int id, String apellidos, String nombre, String fechaNacimiento) {
        this.apellidos = apellidos;
        this.nombre = nombre;
        this.fechaNacimiento = fechaNacimiento;
        this.id = id;
    }

    public Cliente(Cliente cliente) {
        this.apellidos = cliente.getApellidos();
        this.nombre = cliente.getNombre();
        this.fechaNacimiento = cliente.getFechaNacimiento();
    }

    public Cliente() {

    }

    public String getApellidos() {
        return apellidos;
    }

    public void setApellidos(String apellidos) {
        this.apellidos = apellidos;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getFechaNacimiento() {
        return fechaNacimiento;
    }

    public void setFechaNacimiento(String fechaNacimiento) {
        this.fechaNacimiento = fechaNacimiento;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
}

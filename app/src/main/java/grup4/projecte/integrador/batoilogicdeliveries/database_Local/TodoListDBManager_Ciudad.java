package grup4.projecte.integrador.batoilogicdeliveries.database_Local;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import java.util.ArrayList;

import grup4.projecte.integrador.batoilogicdeliveries.Modelos_Internos.Ciudad;


public class TodoListDBManager_Ciudad {
    private TodoListDBHelper todoListDBHelper;

    public TodoListDBManager_Ciudad(Context context) {
        todoListDBHelper = TodoListDBHelper.getInstance(context);
    }

    // Operations

    // CREATE new ros
    public void insertCiudad(Ciudad ciudad) {
        // open database to read and write
        SQLiteDatabase sqLiteDatabase = todoListDBHelper.getWritableDatabase();
        int id;

        String nombre = ciudad.getNombre();

        if (sqLiteDatabase != null) {
            ContentValues contentValue = new ContentValues();


            contentValue.put(TodoListDBContract.Ciudades.NOMBRE, nombre);


            sqLiteDatabase.insert(TodoListDBContract.Ciudades.TABLE_NAME, null, contentValue);

            // getWritableDatabase & getReadableDatabase() are expensive, you should leave your database connection
            // open for as long as you possibly need to access it
            //
            // sqLiteDatabase.close();
        }
    }


    // Get all data from Ciudades table
    public ArrayList<Ciudad> getCiudades() {
        ArrayList<Ciudad> ciudadList = new ArrayList<>();

        // open database to read
        SQLiteDatabase sqLiteDatabase = todoListDBHelper.getReadableDatabase();

        if (sqLiteDatabase != null) {
            String[] projection = new String[]{TodoListDBContract.Ciudades._ID,

                    TodoListDBContract.Ciudades.NOMBRE
            };


            Cursor cursorTodoList = sqLiteDatabase.query(TodoListDBContract.Ciudades.TABLE_NAME,
                    projection,                     // The columns toView return
                    null,                  // no WHERE clause
                    null,               // no values for the WHERE clause
                    null,                   // don't group hte rows
                    null,                    // don't filter by row groups
                    null);                  // don't sort rows


            if (cursorTodoList != null) {
                // get the column indexes for required columns
                int _idIndex = cursorTodoList.getColumnIndexOrThrow(TodoListDBContract.Ciudades._ID);

                int nombreIndex = cursorTodoList.getColumnIndexOrThrow(TodoListDBContract.Ciudades.NOMBRE);


                //read data and add to ArrayList
                while (cursorTodoList.moveToNext()) {
                    Ciudad Ciudad = new Ciudad(cursorTodoList.getInt(_idIndex),

                            cursorTodoList.getString(nombreIndex)
                    );

                    ciudadList.add(Ciudad);
                }
                // close cursor to free resources
                cursorTodoList.close();
            }
        }
        return ciudadList;
    }

    public void close() {
        todoListDBHelper.close();       // closes any opened database object
    }

    /**
     * Aci actualitzem el contingut de cada element al obrir el EditTaskActivity
     */
    public void update(Ciudad ciudad) {
        SQLiteDatabase sqLiteDatabase = todoListDBHelper.getWritableDatabase();
        int id = ciudad.getId();
        String nombre = ciudad.getNombre();
        if (sqLiteDatabase != null) {
            ContentValues contentValue = new ContentValues();

            contentValue.put(TodoListDBContract.Ciudades.NOMBRE, nombre);


            sqLiteDatabase.update(TodoListDBContract.Ciudades.TABLE_NAME, contentValue, "_id=" + id, null);


        }
    }

    public void deleteCiudades(int id) {
        SQLiteDatabase sqLiteDatabase = todoListDBHelper.getWritableDatabase();
        if (sqLiteDatabase != null) {
            sqLiteDatabase.delete(TodoListDBContract.Ciudades.TABLE_NAME, "_id=" + id, null);

        }

    }

    public void deleteComplCiudades() {
        SQLiteDatabase sqLiteDatabase = todoListDBHelper.getWritableDatabase();
        String stat = "Completed";
        if (sqLiteDatabase != null) {
//            sqLiteDatabase.delete(TodoListDBContract.Ciudades.TABLE_NAME, TodoListDBContract.Ciudades.STATUS + "=" + stat, null);
            sqLiteDatabase.execSQL("DELETE FROM " + TodoListDBContract.Ciudades.TABLE_NAME + " WHERE status='Completed'");
        }
    }

    public void deleteAllCiudades() {
        SQLiteDatabase sqLiteDatabase = todoListDBHelper.getWritableDatabase();
        if (sqLiteDatabase != null) {
            sqLiteDatabase.delete(TodoListDBContract.Ciudades.TABLE_NAME, null, null);

        }

    }


    public ArrayList<Ciudad> getSelection(String stat) {
        ArrayList<Ciudad> ciudadList = new ArrayList<>();
        String[] tipus = new String[]{stat};
        System.out.println("\t\t\t---------------->" + stat);

        // open database to read
        SQLiteDatabase sqLiteDatabase = todoListDBHelper.getReadableDatabase();

        if (sqLiteDatabase != null) {
            String[] projection = new String[]{TodoListDBContract.Ciudades._ID,
                    TodoListDBContract.Ciudades.NOMBRE,
            };
//
//            taskList.clear();
            System.out.println(ciudadList.size());
//
            Cursor cursorTodoList = sqLiteDatabase.query(TodoListDBContract.Ciudades.TABLE_NAME,
                    projection,                     // The columns toView return
                    "status=?",                  // no WHERE clause
                    tipus,               // no values for the WHERE clause
                    null,                   // don't group hte rows
                    null,                    // don't filter by row groups
                    null);                  // don't sort rows


            if (cursorTodoList != null) {
                // get the column indexes for required columns
                int _idIndex = cursorTodoList.getColumnIndexOrThrow(TodoListDBContract.Ciudades._ID);
                int nombreIndex = cursorTodoList.getColumnIndexOrThrow(TodoListDBContract.Ciudades.NOMBRE);


                //read data and add to ArrayList
                while (cursorTodoList.moveToNext()) {
                    Ciudad Ciudad = new Ciudad(cursorTodoList.getInt(_idIndex),
                            cursorTodoList.getString(nombreIndex)
                    );
                    ciudadList.add(Ciudad);
                    System.out.println("\t\t" + cursorTodoList.getInt(_idIndex));
                }
                System.out.println("\t\t\t\t\t\t--------->" + ciudadList.size());
                // close cursor to free resources
                cursorTodoList.close();
            }
        }
        return ciudadList;
    }


}

package grup4.projecte.integrador.batoilogicdeliveries.database_Local;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import java.util.ArrayList;

import grup4.projecte.integrador.batoilogicdeliveries.Modelos_Internos.Ruta;


public class TodoListDBManager_Ruta {
    private TodoListDBHelper todoListDBHelper;

    public TodoListDBManager_Ruta(Context context) {
        todoListDBHelper = TodoListDBHelper.getInstance(context);
    }

    // Operations

    // CREATE new ros
    public void insertRuta(Ruta ruta) {
        // open database to read and write
        SQLiteDatabase sqLiteDatabase = todoListDBHelper.getWritableDatabase();
        int id = ruta.getId();
        int repartidor_id = ruta.getRepartidor();
        if (sqLiteDatabase != null) {
            ContentValues contentValue = new ContentValues();
            contentValue.put(TodoListDBContract.Rutas.REPARTIDOR_ID, repartidor_id);


            sqLiteDatabase.insert(TodoListDBContract.Rutas.TABLE_NAME, null, contentValue);

            // getWritableDatabase & getReadableDatabase() are expensive, you should leave your database connection
            // open for as long as you possibly need to access it
            //
            // sqLiteDatabase.close();
        }
    }


    // Get all data from Rutas table
    public ArrayList<Ruta> getRutas() {
        ArrayList<Ruta> RutaList = new ArrayList<>();

        // open database to read
        SQLiteDatabase sqLiteDatabase = todoListDBHelper.getReadableDatabase();

        if (sqLiteDatabase != null) {
            String[] projection = new String[]{TodoListDBContract.Rutas._ID,
                    TodoListDBContract.Rutas.REPARTIDOR_ID};


            Cursor cursorTodoList = sqLiteDatabase.query(TodoListDBContract.Rutas.TABLE_NAME,
                    projection,                     // The columns toView return
                    null,                  // no WHERE clause
                    null,               // no values for the WHERE clause
                    null,                   // don't group hte rows
                    null,                    // don't filter by row groups
                    null);                  // don't sort rows


            if (cursorTodoList != null) {
                // get the column indexes for required columns
                int _idIndex = cursorTodoList.getColumnIndexOrThrow(TodoListDBContract.Rutas._ID);
                int repartidorIndex = cursorTodoList.getColumnIndexOrThrow(TodoListDBContract.Rutas.REPARTIDOR_ID);


                //read data and add to ArrayList
                while (cursorTodoList.moveToNext()) {
                    Ruta Ruta = new Ruta(cursorTodoList.getInt(_idIndex),
                            cursorTodoList.getInt(repartidorIndex));

                    RutaList.add(Ruta);
                }
                // close cursor to free resources
                cursorTodoList.close();
            }
        }
        return RutaList;
    }

    public void close() {
        todoListDBHelper.close();       // closes any opened database object
    }

    /**
     * Aci actualitzem el contingut de cada element al obrir el EditTaskActivity
     *
     * @param ruta
     */
    public void update(Ruta ruta) {
        SQLiteDatabase sqLiteDatabase = todoListDBHelper.getWritableDatabase();
        int id = ruta.getId();
        int repartidor_id = ruta.getRepartidor();
        if (sqLiteDatabase != null) {
            ContentValues contentValue = new ContentValues();

            contentValue.put(TodoListDBContract.Rutas.REPARTIDOR_ID, repartidor_id);

            sqLiteDatabase.update(TodoListDBContract.Rutas.TABLE_NAME, contentValue, "_id=" + id, null);


        }
    }

    public void deleteRutas(int id) {
        SQLiteDatabase sqLiteDatabase = todoListDBHelper.getWritableDatabase();
        if (sqLiteDatabase != null) {
            sqLiteDatabase.delete(TodoListDBContract.Rutas.TABLE_NAME, "_id=" + id, null);

        }

    }

    public void deleteComplRutas() {
        SQLiteDatabase sqLiteDatabase = todoListDBHelper.getWritableDatabase();
        String stat = "Completed";
        if (sqLiteDatabase != null) {
//            sqLiteDatabase.delete(TodoListDBContract.Rutas.TABLE_NAME, TodoListDBContract.Rutas.STATUS + "=" + stat, null);
            sqLiteDatabase.execSQL("DELETE FROM " + TodoListDBContract.Rutas.TABLE_NAME + " WHERE status='Completed'");
        }
    }

    public void deleteAllRutas() {
        SQLiteDatabase sqLiteDatabase = todoListDBHelper.getWritableDatabase();
        if (sqLiteDatabase != null) {
            sqLiteDatabase.delete(TodoListDBContract.Rutas.TABLE_NAME, null, null);

        }

    }


    public ArrayList<Ruta> getSelection(String stat) {
        ArrayList<Ruta> RutaList = new ArrayList<>();
        String[] tipus = new String[]{stat};
        System.out.println("\t\t\t---------------->" + stat);

        // open database to read
        SQLiteDatabase sqLiteDatabase = todoListDBHelper.getReadableDatabase();

        if (sqLiteDatabase != null) {
            String[] projection = new String[]{TodoListDBContract.Rutas._ID,
                    TodoListDBContract.Rutas.REPARTIDOR_ID
            };
//
//            taskList.clear();
            System.out.println(RutaList.size());
//
            Cursor cursorTodoList = sqLiteDatabase.query(TodoListDBContract.Rutas.TABLE_NAME,
                    projection,                     // The columns toView return
                    "status=?",                  // no WHERE clause
                    tipus,               // no values for the WHERE clause
                    null,                   // don't group hte rows
                    null,                    // don't filter by row groups
                    null);                  // don't sort rows


            if (cursorTodoList != null) {
                // get the column indexes for required columns
                int _idIndex = cursorTodoList.getColumnIndexOrThrow(TodoListDBContract.Rutas._ID);
                int repartidorIndex = cursorTodoList.getColumnIndexOrThrow(TodoListDBContract.Rutas.REPARTIDOR_ID);


                //read data and add to ArrayList
                while (cursorTodoList.moveToNext()) {
                    Ruta ruta = new Ruta(cursorTodoList.getInt(_idIndex),
                            cursorTodoList.getInt(repartidorIndex));

                    RutaList.add(ruta);
                    System.out.println("\t\t" + cursorTodoList.getInt(_idIndex));
                }
                System.out.println("\t\t\t\t\t\t--------->" + RutaList.size());
                // close cursor to free resources
                cursorTodoList.close();
            }
        }
        return RutaList;
    }


}

package grup4.projecte.integrador.batoilogicdeliveries.database_Local;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import java.util.ArrayList;

import grup4.projecte.integrador.batoilogicdeliveries.Modelos_Internos.Cliente;


public class TodoListDBManager_Cliente {
    private TodoListDBHelper todoListDBHelper;

    public TodoListDBManager_Cliente(Context context) {
        todoListDBHelper = TodoListDBHelper.getInstance(context);
    }

    // Operations

    // CREATE new ros
    public void insertCliente(Cliente cliente) {
        // open database to read and write
        SQLiteDatabase sqLiteDatabase = todoListDBHelper.getWritableDatabase();
        int id;
        String apellidos = cliente.getApellidos();
        String nombre = cliente.getNombre();
        String fechaNacimiento = cliente.getFechaNacimiento();
        if (sqLiteDatabase != null) {
            ContentValues contentValue = new ContentValues();

            contentValue.put(TodoListDBContract.Clientes.APELLIDOS, apellidos);
            contentValue.put(TodoListDBContract.Clientes.NOMBRE, nombre);
            contentValue.put(TodoListDBContract.Clientes.FECHANACIMIENTO, fechaNacimiento);


            sqLiteDatabase.insert(TodoListDBContract.Clientes.TABLE_NAME, null, contentValue);

            // getWritableDatabase & getReadableDatabase() are expensive, you should leave your database connection
            // open for as long as you possibly need to access it
            //
            // sqLiteDatabase.close();
        }
    }


    // Get all data from Clientes table
    public ArrayList<Cliente> getClientes() {
        ArrayList<Cliente> ClienteList = new ArrayList<>();

        // open database to read
        SQLiteDatabase sqLiteDatabase = todoListDBHelper.getReadableDatabase();

        if (sqLiteDatabase != null) {
            String[] projection = new String[]{TodoListDBContract.Clientes._ID,
                    TodoListDBContract.Clientes.APELLIDOS,
                    TodoListDBContract.Clientes.NOMBRE,
                    TodoListDBContract.Clientes.FECHANACIMIENTO
            };


            Cursor cursorTodoList = sqLiteDatabase.query(TodoListDBContract.Clientes.TABLE_NAME,
                    projection,                     // The columns toView return
                    null,                  // no WHERE clause
                    null,               // no values for the WHERE clause
                    null,                   // don't group hte rows
                    null,                    // don't filter by row groups
                    null);                  // don't sort rows


            if (cursorTodoList != null) {
                // get the column indexes for required columns
                int _idIndex = cursorTodoList.getColumnIndexOrThrow(TodoListDBContract.Clientes._ID);
                int apellidosIndex = cursorTodoList.getColumnIndexOrThrow(TodoListDBContract.Clientes.APELLIDOS);
                int nombreIndex = cursorTodoList.getColumnIndexOrThrow(TodoListDBContract.Clientes.NOMBRE);
                int fechanacimientoIndex = cursorTodoList.getColumnIndexOrThrow(TodoListDBContract.Clientes.FECHANACIMIENTO);

                //read data and add to ArrayList
                while (cursorTodoList.moveToNext()) {
                    Cliente cliente = new Cliente(cursorTodoList.getInt(_idIndex),
                            cursorTodoList.getString(apellidosIndex),
                            cursorTodoList.getString(nombreIndex),
                            cursorTodoList.getString(fechanacimientoIndex)
                    );

                    ClienteList.add(cliente);
                }
                // close cursor to free resources
                cursorTodoList.close();
            }
        }
        return ClienteList;
    }

    public void close() {
        todoListDBHelper.close();       // closes any opened database object
    }

    /**
     * Aci actualitzem el contingut de cada element al obrir el EditTaskActivity
     */
    public void update(Cliente cliente) {
        SQLiteDatabase sqLiteDatabase = todoListDBHelper.getWritableDatabase();
        int id = cliente.getId();
        String apellidos = cliente.getApellidos();
        String nombre = cliente.getNombre();
        String fechaNacimiento = cliente.getFechaNacimiento();;
        if (sqLiteDatabase != null) {
            ContentValues contentValue = new ContentValues();

            contentValue.put(TodoListDBContract.Clientes.APELLIDOS, apellidos);
            contentValue.put(TodoListDBContract.Clientes.NOMBRE, nombre);
            contentValue.put(TodoListDBContract.Clientes.FECHANACIMIENTO, fechaNacimiento);


            sqLiteDatabase.update(TodoListDBContract.Clientes.TABLE_NAME, contentValue, "_id=" + id, null);


        }
    }

    public void deleteClientes(int id) {
        SQLiteDatabase sqLiteDatabase = todoListDBHelper.getWritableDatabase();
        if (sqLiteDatabase != null) {
            sqLiteDatabase.delete(TodoListDBContract.Clientes.TABLE_NAME, "_id=" + id, null);

        }

    }

    public void deleteComplClientes() {
        SQLiteDatabase sqLiteDatabase = todoListDBHelper.getWritableDatabase();
        String stat = "Completed";
        if (sqLiteDatabase != null) {
//            sqLiteDatabase.delete(TodoListDBContract.Clientes.TABLE_NAME, TodoListDBContract.Clientes.STATUS + "=" + stat, null);
            sqLiteDatabase.execSQL("DELETE FROM " + TodoListDBContract.Clientes.TABLE_NAME + " WHERE status='Completed'");
        }
    }

    public void deleteAllClientes() {
        SQLiteDatabase sqLiteDatabase = todoListDBHelper.getWritableDatabase();
        if (sqLiteDatabase != null) {
            sqLiteDatabase.delete(TodoListDBContract.Clientes.TABLE_NAME, null, null);

        }

    }


    public ArrayList<Cliente> getSelection(String stat) {
        ArrayList<Cliente> ClienteList = new ArrayList<>();
        String[] tipus = new String[]{stat};
        System.out.println("\t\t\t---------------->" + stat);

        // open database to read
        SQLiteDatabase sqLiteDatabase = todoListDBHelper.getReadableDatabase();

        if (sqLiteDatabase != null) {
            String[] projection = new String[]{TodoListDBContract.Clientes._ID,
                    TodoListDBContract.Clientes.APELLIDOS,
                    TodoListDBContract.Clientes.NOMBRE,
                    TodoListDBContract.Clientes.FECHANACIMIENTO
            };
//
//            taskList.clear();
            System.out.println(ClienteList.size());
//
            Cursor cursorTodoList = sqLiteDatabase.query(TodoListDBContract.Clientes.TABLE_NAME,
                    projection,                     // The columns toView return
                    "status=?",                  // no WHERE clause
                    tipus,               // no values for the WHERE clause
                    null,                   // don't group hte rows
                    null,                    // don't filter by row groups
                    null);                  // don't sort rows


            if (cursorTodoList != null) {
                // get the column indexes for required columns
                int _idIndex = cursorTodoList.getColumnIndexOrThrow(TodoListDBContract.Clientes._ID);
                int apellidosIndex = cursorTodoList.getColumnIndexOrThrow(TodoListDBContract.Clientes.APELLIDOS);
                int nombreIndex = cursorTodoList.getColumnIndexOrThrow(TodoListDBContract.Clientes.NOMBRE);
                int fechanacimientoIndex = cursorTodoList.getColumnIndexOrThrow(TodoListDBContract.Clientes.FECHANACIMIENTO);


                //read data and add to ArrayList
                while (cursorTodoList.moveToNext()) {
                    Cliente cliente = new Cliente(cursorTodoList.getInt(_idIndex),
                            cursorTodoList.getString(apellidosIndex),
                            cursorTodoList.getString(nombreIndex),
                            cursorTodoList.getString(fechanacimientoIndex)
                    );
                    ClienteList.add(cliente);
                    System.out.println("\t\t" + cursorTodoList.getInt(_idIndex));
                }
                System.out.println("\t\t\t\t\t\t--------->" + ClienteList.size());
                // close cursor to free resources
                cursorTodoList.close();
            }
        }
        return ClienteList;
    }


}

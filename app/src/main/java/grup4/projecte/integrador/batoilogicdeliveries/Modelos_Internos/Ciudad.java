package grup4.projecte.integrador.batoilogicdeliveries.Modelos_Internos;

import java.io.Serializable;

public class Ciudad implements Serializable {

    public int id;
    public String nombre;

    public Ciudad(int id, String nombre) {
        this.id = id;
        this.nombre = nombre;
    }

    public Ciudad() {

    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }
}

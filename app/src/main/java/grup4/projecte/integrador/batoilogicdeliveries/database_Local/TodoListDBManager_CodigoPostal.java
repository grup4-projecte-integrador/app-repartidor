package grup4.projecte.integrador.batoilogicdeliveries.database_Local;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import java.util.ArrayList;

import grup4.projecte.integrador.batoilogicdeliveries.Modelos_Internos.Cliente;
import grup4.projecte.integrador.batoilogicdeliveries.Modelos_Internos.CodigoPostal;


public class TodoListDBManager_CodigoPostal {
    private TodoListDBHelper todoListDBHelper;

    public TodoListDBManager_CodigoPostal(Context context) {
        todoListDBHelper = TodoListDBHelper.getInstance(context);
    }

    // Operations

    // CREATE new ros
    public void insertCodigoPostal(CodigoPostal codigoPostal) {
        // open database to read and write
        SQLiteDatabase sqLiteDatabase = todoListDBHelper.getWritableDatabase();
        int id;
        String codigo = codigoPostal.getCodigo();
        int ciudad = codigoPostal.getCiudad();

        if (sqLiteDatabase != null) {
            ContentValues contentValue = new ContentValues();


            contentValue.put(TodoListDBContract.Codigo_Postal.NUMERO, codigo);
            contentValue.put(TodoListDBContract.Codigo_Postal.CIUDAD_ID, ciudad);


            sqLiteDatabase.insert(TodoListDBContract.Codigo_Postal.TABLE_NAME, null, contentValue);

            // getWritableDatabase & getReadableDatabase() are expensive, you should leave your database connection
            // open for as long as you possibly need to access it
            //
            // sqLiteDatabase.close();
        }
    }


    // Get all data from CodigoPostal table
    public ArrayList<CodigoPostal> getCodigoPostal() {
        ArrayList<CodigoPostal> CodigoPostalList = new ArrayList<>();

        // open database to read
        SQLiteDatabase sqLiteDatabase = todoListDBHelper.getReadableDatabase();

        if (sqLiteDatabase != null) {
            String[] projection = new String[]{TodoListDBContract.Codigo_Postal._ID,
                    TodoListDBContract.Codigo_Postal.NUMERO,
                    TodoListDBContract.Codigo_Postal.CIUDAD_ID
            };


            Cursor cursorTodoList = sqLiteDatabase.query(TodoListDBContract.Codigo_Postal.TABLE_NAME,
                    projection,                     // The columns toView return
                    null,                  // no WHERE clause
                    null,               // no values for the WHERE clause
                    null,                   // don't group hte rows
                    null,                    // don't filter by row groups
                    null);                  // don't sort rows


            if (cursorTodoList != null) {
                // get the column indexes for required columns
                int _idIndex = cursorTodoList.getColumnIndexOrThrow(TodoListDBContract.Codigo_Postal._ID);
                int codigoIndex = cursorTodoList.getColumnIndexOrThrow(TodoListDBContract.Codigo_Postal.NUMERO);
                int ciudadIndex = cursorTodoList.getColumnIndexOrThrow(TodoListDBContract.Codigo_Postal.CIUDAD_ID);

                //read data and add to ArrayList
                while (cursorTodoList.moveToNext()) {
                    CodigoPostal codigoPostal = new CodigoPostal(cursorTodoList.getInt(_idIndex),
                            cursorTodoList.getString(codigoIndex),
                            cursorTodoList.getInt(ciudadIndex)
                    );

                    CodigoPostalList.add(codigoPostal);
                }
                // close cursor to free resources
                cursorTodoList.close();
            }
        }
        return CodigoPostalList;
    }

    public void close() {
        todoListDBHelper.close();       // closes any opened database object
    }

    /**
     * Aci actualitzem el contingut de cada element al obrir el EditTaskActivity
     */
    public void update(CodigoPostal codigoPostal) {
        SQLiteDatabase sqLiteDatabase = todoListDBHelper.getWritableDatabase();
        int id = codigoPostal.getId();
        String codigo = codigoPostal.getCodigo();
        int ciudad = codigoPostal.getCiudad();

        if (sqLiteDatabase != null) {
            ContentValues contentValue = new ContentValues();

            contentValue.put(TodoListDBContract.Codigo_Postal.NUMERO, codigo);
            contentValue.put(TodoListDBContract.Codigo_Postal.CIUDAD_ID, ciudad);


            sqLiteDatabase.update(TodoListDBContract.Codigo_Postal.TABLE_NAME, contentValue, "_id=" + id, null);


        }
    }

    public void deleteCodigoPostal(int id) {
        SQLiteDatabase sqLiteDatabase = todoListDBHelper.getWritableDatabase();
        if (sqLiteDatabase != null) {
            sqLiteDatabase.delete(TodoListDBContract.Codigo_Postal.TABLE_NAME, "_id=" + id, null);

        }

    }

    public void deleteComplCodigoPostal() {
        SQLiteDatabase sqLiteDatabase = todoListDBHelper.getWritableDatabase();
        String stat = "Completed";
        if (sqLiteDatabase != null) {
//            sqLiteDatabase.delete(TodoListDBContract.CodigoPostal.TABLE_NAME, TodoListDBContract.CodigoPostal.STATUS + "=" + stat, null);
            sqLiteDatabase.execSQL("DELETE FROM " + TodoListDBContract.Codigo_Postal.TABLE_NAME + " WHERE status='Completed'");
        }
    }

    public void deleteAllCodigoPostal() {
        SQLiteDatabase sqLiteDatabase = todoListDBHelper.getWritableDatabase();
        if (sqLiteDatabase != null) {
            sqLiteDatabase.delete(TodoListDBContract.Codigo_Postal.TABLE_NAME, null, null);

        }

    }


    public ArrayList<CodigoPostal> getSelection(String stat) {
        ArrayList<CodigoPostal> CodigoPostalList = new ArrayList<>();
        String[] tipus = new String[]{stat};
        System.out.println("\t\t\t---------------->" + stat);

        // open database to read
        SQLiteDatabase sqLiteDatabase = todoListDBHelper.getReadableDatabase();

        if (sqLiteDatabase != null) {
            String[] projection = new String[]{TodoListDBContract.Codigo_Postal._ID,
                    TodoListDBContract.Codigo_Postal.NUMERO,
                    TodoListDBContract.Codigo_Postal.CIUDAD_ID
            };
//
//            taskList.clear();
            System.out.println(CodigoPostalList.size());
//
            Cursor cursorTodoList = sqLiteDatabase.query(TodoListDBContract.Codigo_Postal.TABLE_NAME,
                    projection,                     // The columns toView return
                    "status=?",                  // no WHERE clause
                    tipus,               // no values for the WHERE clause
                    null,                   // don't group hte rows
                    null,                    // don't filter by row groups
                    null);                  // don't sort rows


            if (cursorTodoList != null) {
                // get the column indexes for required columns
                int _idIndex = cursorTodoList.getColumnIndexOrThrow(TodoListDBContract.Codigo_Postal._ID);
                int codigoIndex = cursorTodoList.getColumnIndexOrThrow(TodoListDBContract.Codigo_Postal.NUMERO);
                int ciudadIndex = cursorTodoList.getColumnIndexOrThrow(TodoListDBContract.Codigo_Postal.CIUDAD_ID);

                //read data and add to ArrayList
                while (cursorTodoList.moveToNext()) {
                    CodigoPostal CodigoPostal = new CodigoPostal(cursorTodoList.getInt(_idIndex),
                            cursorTodoList.getString(codigoIndex),
                            cursorTodoList.getInt(ciudadIndex));

                    CodigoPostalList.add(CodigoPostal);
                    System.out.println("\t\t" + cursorTodoList.getInt(_idIndex));
                }
                System.out.println("\t\t\t\t\t\t--------->" + CodigoPostalList.size());
                // close cursor to free resources
                cursorTodoList.close();
            }
        }
        return CodigoPostalList;
    }


}
